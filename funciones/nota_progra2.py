# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

#descripció : nota de progra

#Especificacions d'entrada i joc de proves: EE: floats

import sys

#leemos las entradas por argumento y las convertimos de cadena a float
n1 = float(sys.argv[1])
n2 = float(sys.argv[2])
n3 = float(sys.argv[3])
n4 = float(sys.argv[4])
n5 = float(sys.argv[5])
n6 = float(sys.argv[6])


#definimos la funcion
def nota_programacion(n1,n2,n3,n4,n5,n6):
	'''
	e: 6 notas floats
	s: 1 float o 1 entero
	'''
	menos_impor = (n1+n6) / 2
	return int((menos_impor + n2 + n3 + n4 + n5) / 5)

print(nota_programacion(n1,n2,n3,n4,n5,n6))
	

