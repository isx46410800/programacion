# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

def suma(valor1, valor2, valor3):
	return valor1 + valor2 + valor3

resultado = suma(10,20,30)
print(resultado)

def division(valor1, valor2):
	return valor1 / valor2

resultado = division(100,10)
print(resultado)

#segunda forma tomando solo un valor concreto si no pasamos los dos argumentos
def division(valor1, valor2 = 10):
	return valor1 / valor2

resultado2 = division(100)
print(resultado2)
