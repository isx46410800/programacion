# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

#calcular lletra dni

#definimos funcion calcular letra
def lletra_dni(dni):
   '''
    Entrada: un string
    Sortida: un string(caracter)
   '''
   LLETRES = 'TRWAGMYFPDXBNJZSQVHLCKE'
   return LLETRES[int(dni) % 23]


#definimos funcion que indique si el dni es correcte
def es_dni_correcte(cadena):
	'''
	entrada: un string
	sortida: un boleano
	'''
	#miramos que longitud sea 9
	if len(cadena) != 9:
		return False
	#8 primeros sean sumeros
	if not cadena[:-1].isdigit():
		return False

	#Miramos si ultima letra es correcta, llamamos a la funcion letra, recortamos y comparamos con la entrada
	return lletra_dni(cadena[:-1]) == cadena[-1]

#mostramos resultados
print(lletra_dni('46410800'))
print(es_dni_correcte('46410800C'))


