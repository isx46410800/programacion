def es_perfecte(num):
    
    #docstring:
    #entrada int
    #sortida bool

    #declarem les variables necessaries

    comptador = 1
    suma = 0
    n_perfecte = False

    #fem un bucle per calcular els numeros perfectes

    while comptador < num:

        if num%comptador==0:

            suma = suma + comptador

        comptador += 1
    
    if suma == num:

        n_perfecte = True

    #retornem n_perfecte

    return n_perfecte

#-----JOC DE PROVES-----------

#input------------------output

print(es_perfecte(6))     #true
print(es_perfecte(28))    #true
print(es_perfecte(30))    #false
