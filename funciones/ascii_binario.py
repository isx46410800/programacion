# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : codificar de binari a text ASCII

#Especificacions d'entrada i joc de proves: EE: cadena binaria de byte
#recullo l'examen a les 13:00 en punt. 	0111001001100101011000110111010101101100011011000110111100100000011011000010011101100101011110000110000101101101011
#0010101101110001000000110000100100000011011000110010101110011001000000011000100110011001110100011000000110000001000000110010101101110001000000111000001110101011011100111010000101110

#funcion de decimal a binario
def deci_to_bin(numero):
	'''
	i:int
	o:cadena de 8 bits 
	'''
	
	
#funcion para pasar de ASCII a decimal
def ascii_to_deci(cadena):
	'''
	i:cadena
	o:int
	'''
	suma=0
	for i in cadena:
		numero = ord(i)
		suma = suma + numero
	return suma
	
print(ascii_to_deci('hola'))
	
cadena = input()
