# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

def division(valor1, valor2):
	return valor1 / valor2

resultado = division(100,10)
print(resultado)

def multiples_valores():
	return "string", 1, True, 25.5

#podemos asignar una funcion dentro de una funcion
def mostrar_resultado(mi_funcion):
	print(mi_funcion(100,10))

mi_variable = division
mostrar_resultado(mi_variable)


