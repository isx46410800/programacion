# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : decir si un numero es perfecto o no

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER > 0

#Joc de proves
#Entrada 	Sortida 
#1		F
#2		F
#3		F
#4		F
#5		F
#6		T
#10		F

#importamos para pasar argumentos
import sys

#definimos la funcion es_perfecte
def es_perfecte(numero):
	'''
	Entrada: Entero
	Sortida: Booleano
	'''
	#iniciamos un contador de divisores
	i=1

	#suma de divisores
	suma_perfecta = 0

	#bucle para ir mirando divisores
	while i < numero:
		#si es divisor, lo sumamos
		if numero % i == 0:
			#print(i)
			suma_perfecta = suma_perfecta + i
		#miramos el siguiente
		i = i + 1

	#Mostramos resultados
	return suma_perfecta == numero
#	if suma_perfecta == numero:
#		return True
#	else:
#		return False

#print(es_perfecte(8128))

################ CONTROL D'ERRORS ################
MISSATGE = "USAGE: Error d'arguments, és: PROG ARGUMENT"

if len(sys.argv) != 2:
	print(MISSATGE)
	exit (1)




################ PROGRAMA ################

#Cantidad de numeros perfectos
n_perfectos = sys.argv[1]

#Iniciamos desde que numero empezamos a mirar
n = 1

#Contador de numeros perfectos
cont = 0

#bucle
#mientras el numero del contador sea mas pequeño o igual a la cantidad de perfectos
while cont < int(n_perfectos):
	#llamamos a la funcion y la guardamos en una variable
	resultado = es_perfecte(n)
	#es_perfecte(n)
	
	#Si es perfecto, sumamos al contador de perfectos y lo mostramos
	if resultado == True:
	#if es_perfecte(n) == True:
		cont = cont + 1
		print(n)
	
	#Miramos el siguiente numero
	n = n + 1










