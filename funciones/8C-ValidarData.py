# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si una fecha sigue el formato dd/mm/aaaa

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import sys

cadena = sys.argv[1]

#definimos funcion validar formato de una fecha
def validar_formato(cadena):
	'''
	entrada: str
	sortida: booleano
	'''

	#longitud correcte
	if len(cadena) != 10:
		return False
	#barras correctas
	if cadena[2] != '/' and cadena[5] != '/':
		return False
	#numeros correctos
	if not (cadena[0:2].isdigit() and cadena[3:5].isdigit() and cadena[6:10].isdigit()):
		return False
	
	return True

print(validar_formato(cadena))
	
