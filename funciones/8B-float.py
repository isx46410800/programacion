# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció 6- Fer un programa que rebi un número per paràmetre i ens digui si és un número sencer, un número real o no és un número correcte. 
#Exemple: 
#	user-bash$ python 10validar.py 56     --> Treu per pantalla: el 56 és un número sencer 
#	user-bash$ python 10validar.py 52.78     --> Treu per pantalla: el 52.78 és un número real 
#	user-bash$ python 10validar.py 52AA78     --> Treu per pantalla: el 52AA78 no és un número 

#especificacions d'entrada: una cadena de entrada sin espacios

#Joc de proves
#Entrada 	Sortida
#56		ENTERO
#52.78		REAL
#AA33		NO ES NUMERO
#+5		ENTERO
#+-5		NO ES NUMERO
#5-3		NO ES NUMERO
#.		no
#.5		real
#33.-55		no

#si es entero
import sys

#introducimos el numero
cadena = sys.argv[1]


#funcion es entero
#definimos la funcion es entero
def es_entero(cadena):
	'''
	e: cadena
	s: booleano
	'''
	#quitamos signo si hace falta
	if cadena[0] in '+-':
		cadena = cadena[1:]
	#si todo son digitos
	return cadena.isdigit()

#llamamos a la funcion
#print(es_entero(cadena))

#funcion si es float
def es_float(cadena):
	'''
	e: cadena
	s: booleano
	'''
	#Si no, si es float
	if es_entero(cadena) == False:
		es_float = False
		contador_puntos=0
		contador_digitos=0
		#caracter a caracter
		for c in cadena:
			#Si es un punt
			if cadena=='.':
				#Contamos
				contador_puntos=contador_puntos+1
			#Si es un digito
			elif cadena>='0' and cadena<='9':
				#Contamos
				contador_digitos=contador_digitos+1
			#Sino
			else:
				es_float = False
				
		#Si ha mas de un punto
		if contador_puntos>1:
				es_float = False 

		#Si ha mas de un digito
		if contador_digitos<1:
				es_float = False 

#Mostramos resultados
print(es_float(cadena))



