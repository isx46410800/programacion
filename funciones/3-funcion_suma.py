# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

#definimos la funcion suma
def suma(valor1,valor2):
	return valor1 + valor2

#Mostramos resultados
resultado = suma(2,2)
print(resultado)

resultado = suma(3,6)
print(resultado)

resultado = suma(1,2)
print(resultado)
