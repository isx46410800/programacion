# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

#definimos la funcion quadrat
def quadrat(numero):
	#calculamos el cuadrado asignado a una variable
	cuadrado = numero * numero
	#nos retorna el valor a la funcion definida
	return cuadrado

#mostramos resultados pasando el argumento
resultado = quadrat(2)
print(resultado)
resultado = quadrat(5)
print(resultado)
resultado = quadrat(6)
print(resultado)

