# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor
#isx
#data 
#versió


#descripció: Programa que fa la mitjana de les notes
#especificacions d'entrada: un nombre enter positiu.
#Joc de proves
#--------------------------------------------------------
#	Entrada					Sortida
#--------------------------------------------------------
#	  2						  3
#	  5						  11
#	  6						  11


def es_primer(n):
	'''docstring'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 

def es_sophie_germain_primer(a):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	Input: Int
	Output: Bool
	'''
	#Comprovem que el número actual és un nombre primer
	if es_primer(a):
		#Si és primer, comprovar que el seu número segur també és primer
		safenum = 2 * a + 1
		return es_primer(safenum)
	return False
		
def seguent_sophie_primer(n):
	'''
	Funció que donat un enter qualsevol, ens retorna el primer nombre més 
	gran que acompleix la condició dels primers de Sophie Germain 
	Input: Int
	Output: Int
	'''
#Llegim el nombre introduït i creem una variable que sumi 1 al nombre introduït
	suma = n + 1
	#Utilitzem un bucle i la comprovació del número de Sophie per trobar el següent número
	valor = False
	while valor == False:
		if es_sophie_germain_primer(suma):
			valor = True
		else:
			suma = suma + 1
	return suma

print(seguent_sophie_primer(2))
print(seguent_sophie_primer(5))
print(seguent_sophie_primer(6))
