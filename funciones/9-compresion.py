# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

#descripció : nota de progra

#Especificacions d'entrada i joc de proves: EE: floats

#9. - Crea una funció que donada una cadena la retorni comprimida, utilitzant l'algoritme de compressió de #l'examen de l'any passat (és l'enunciat de l'examen 5 que teniu a la carpeta examensAnteriors) .

#Exemple:
#	Entrada:						Sortida:
#queeeee     faaaaaaacil qqque ees		que facil que es



#entrada
cadena = input()

#definimos funcion para eliminar eliminacion de caracteres
def eliminar_rep(cadena):
	'''
	entrada: string
	sortida: string
	'''

	cad_nova = ''
	cad_anterior = ''

	#recorremos caracter a caracter de la entrada que pongamos
	for c in cadena:	
		#Si no coincide el caracter con el caracter anterior, la cadena nueva queda lo que habia + c
		if c != cad_anterior:
			cad_nova = cad_nova + c
		#Añadimos que el anterior es el c de ahora	
		cad_anterior = c
		
	return cad_nova

#llamamos la funcion
print(eliminar_rep(cadena))

