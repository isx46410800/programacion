# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : nota de progra

#Especificacions d'entrada i joc de proves: EE: floats

import sys


#definimos la funcion calculo de nota
def nota_programacion(n1,n2,n3,n4,n5,n6):
	'''
	e: 6 notas floats
	s: 1 float o 1 entero
	'''
	menos_impor = int(n1+n6) / 2
	return int(menos_impor + n2 + n3 + n4 + n5) / 5
	#return int((menos_impor + n2 + n3 + n4 + n5) / 5) para dar la nota con num entero

#definimos funcion float
def es_float(cadena):
	'''
	e: cadena
	s: booleano
	'''
	flotante = True
	contador_puntos = 0
	contador_digitos = 0
	#quitamos signo si hace falta
	if cadena[0] in '+-':
		cadena = cadena[1:]
	
	#miramos cada caracter
	for c in cadena:
		#Si es un punto
		if c == '.':
			contador_puntos = contador_puntos + 1
		#Si es un digito
		elif c.isdigit():
			contador_digitos = contador_digitos + 1
		#es otra cosa
		else:
			flotante = False

	#si hay mas de un punto
	if contador_puntos>1:
		flotante = False
	#si hay menos de un digito
	if contador_digitos<1:
		flotante = False
	#Nos retorna el valor final de la variable
	return flotante

#resultado al llamar la funcion
#print(es_float(cadena))

##########CONTROL ERRORES##############
MISSATGE = "USAGE: arguments incorrecte o no son floats. PROG + 6 arguments floats"
MISS_FLOAT = "USAGE: no es un float"

#que tengas los argumentos correctos(6)
if len(sys.argv) != 7:
	print(MISSATGE)
	exit (1)
	
#mirar que cada entrada sea float
#if not es_float(n1):
#	print(MISS_FLOAT)
#	exit (1)


#########PROGRAMA######################
#argumentos
n1 = float(sys.argv[1])
n2 = float(sys.argv[2])
n3 = float(sys.argv[3])
n4 = float(sys.argv[4])
n5 = float(sys.argv[5])
n6 = float(sys.argv[6])

#llamamos a la funcion con los argumentos
print(nota_programacion(n1,n2,n3,n4,n5,n6))
	

