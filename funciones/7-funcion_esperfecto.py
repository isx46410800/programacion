# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : decir si un numero es perfecto o no

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER > 0

#Joc de proves
#Entrada 	Sortida 
#1		F
#2		F
#3		F
#4		F
#5		F
#6		T
#10		F


#definimos la funcion es_perfecte
def es_perfecte(numero):
	'''
	Entrada: Entero
	Sortida: Booleano
	'''
	#iniciamos un contador de divisores
	i=1

	#suma de divisores
	suma_perfecta = 0

	#bucle para ir mirando divisores
	while i < numero:
		#si es divisor, lo sumamos
		if numero % i == 0:
			#print(i)
			suma_perfecta = suma_perfecta + i
		#miramos el siguiente
		i = i + 1

	#Mostramos resultados
	return suma_perfecta == numero
#	if suma_perfecta == numero:
#		return True
#	else:
#		return False

print(es_perfecte(8128))
