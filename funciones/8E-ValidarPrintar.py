# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si una fecha sigue el formato dd/mm/aaaa

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import sys


#definimos funcion validar formato de una fecha
def validar_formato(cadena):
	'''
	entrada: str
	sortida: booleano
	'''

	#longitud correcte
	if len(cadena) != 10:
		return False
	#barras correctas
	if cadena[2] != '/' and cadena[5] != '/':
		return False
	#numeros correctos
	if not (cadena[0:2].isdigit() and cadena[3:5].isdigit() and cadena[6:10].isdigit()):
		return False
	
	return True

#definimos para saber el dia
def getdia(cadena):
	'''
	e: cadena
	s: entero
	'''
	dia = cadena[0] + cadena[1]
	return int(dia)

#definimos para saber el mes
def getmes(cadena):
	'''
	e: cadena
	s: entero
	'''
	mes = cadena[3] + cadena[4]
	return int(mes)

#definimos para saber el año
def getano(cadena):
	'''
	e: cadena
	s: enteros
	'''
	ano = cadena[6] + cadena[7] + cadena[8]+ cadena[9]
	return int(ano)

#PROGRAMA
#entrada
cadena = sys.argv[1]

#primero validamos que sea una fecha formato correcto
if not validar_formato(cadena):
	print("No es fecha correcta")
else:	
	print(validar_formato(cadena))
	print(getdia(cadena))
	print(getmes(cadena))
	print(getano(cadena))




	
