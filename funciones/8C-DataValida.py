# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si una fecha sigue el formato dd/mm/aaaa

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import sys


#leemos argumento
data = sys.argv[1]

#definimos la funcion fecha valida
def es_fecha(data):
	'''
	e: cadena
	s: booleano
	'''
	return data[0].isdigit() and data[1].isdigit() and data[3].isdigit() and data[4].isdigit() and data[6].isdigit() and data[7].isdigit() and data[8].isdigit() and data[9].isdigit() and data[2] == '/' and data[5] == '/'

#llamamos a la funcion	
print(es_fecha(data))
