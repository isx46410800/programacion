# !/usr/bin/python
# -*-coding: utf-8-*-

#CAPÇALERA COMPLERTA O SERÂ UN ZERO
#Mòdul de funcions que treballen amb nombres primers
#Has de completar TOTS els docstrings
	
n = 5
	
def es_primer(n):
	'''docstring'''
	'''
	entrada: numero entero
	sortida: booleano
	'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 
	
def es_sophie_germain_primer(n):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	Entrada: Entero
	Salida: Booleano
	'''
	if es_primer(n):
		s_primer = 2 * n + 1
		i = 2
		sophie = True
		while i<=s_primer and sophie:
			if s_primer%i==0:
				sophie = False
			i = i + 1
	return sophie
		

def seguent_sophie_primer(n):
	'''
	Funció que donat un enter qualsevol, ens retorna el primer nombre més 
	gran que acompleix la condició dels primers de Sophie Germain 
	'''
	
	
