# !/usr/bin/python
# -*-coding: utf-8-*-

#CAPÇALERA COMPLERTA O SERÂ UN ZERO
#Mòdul de funcions que treballen amb nombres primers
#Has de completar TOTS els docstrings

import sys
	
def es_primer(n):
	'''docstring'''
	'''
	entrada: numero entero
	sortida: booleano
	'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 
	
#print(es_primer(2)) T
#print(es_primer(4)) F
#print(es_primer(5)) T
#print(es_primer(7)) T 
#print(es_primer(10)) F

def es_sophie_germain_primer(n):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	Entrada: Entero
	Salida: Booleano
	'''
	p = 0
	if es_primer(n):
		p = 2 * n + 1
	return (es_primer(p))

#print(es_sophie_germain_primer(2)) T
#print(es_sophie_germain_primer(4)) F
#print(es_sophie_germain_primer(5)) T 
#print(es_sophie_germain_primer(7)) F


#Programa que ens calcula els n primers primers de Sophie Germain

#llegir el nombre de primers que volem
quant_primers = 100
#n = 2	#el primer primer és el dos

#bucle (hem de repetir quant_primers vegades)
for i in range(0, quant_primers):
	if es_primer(i) and es_sophie_germain_primer(i):
		mensaje = "s. germain primer: %d , primer segur: %d" % (i, 2 * i + 1)
		print(mensaje)







