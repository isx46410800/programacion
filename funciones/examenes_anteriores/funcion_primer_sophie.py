	
def es_primer(n):
	'''docstring'''
	'''
	entrada: numero entero
	sortida: booleano
	'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 

print(es_primer(2))
print(es_primer(4))
print(es_primer(5))
print(es_primer(7))
print(es_primer(10))
