# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

def division(valor1, valor2):
	return valor1 / valor2

resultado = division(100,10)
print(resultado)

def multiples_valores():
	return "string", 1, True, 25.5

resultado = multiples_valores()
print(resultado)

#coge cada posicion del RETURN para el resultado
string = resultado[0]
print(string)
entero = resultado[1]
print(entero)
booleano = resultado[2]
print(booleano)
floa = resultado[3]
print(floa)

#tambien se pueden asignar directamente con:
string, entero, booleano, floa = multiples_valores()
print(string)
print(entero)
print(booleano)
print(floa)

#podemos asignar una funcion a una variable
mi_variable = division
resultado = mi_variable(100,10)
print(resultado)

#print(division(100,10))


