#funcion para pasar de binario a decimal
def bin_to_deci(num):
	'''
	input: cadena binaria de 8
	output: int
	'''
	transformacion=0
	suma = 0
	exponente = len(num)-1
	#bucle para cada numerito pasarlo a decimal
	for i in num:
		transformacion = 2**exponente * int(i)
		suma = suma + transformacion
		exponente = exponente -1
		
	return suma
	
print(bin_to_deci("11111111"))
print(bin_to_deci("00000001"))
print(bin_to_deci("0100"))
