# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : decir si un numero es perfecto o no

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER > 0

#Joc de proves
#Entrada 	Sortida 
#1		F
#2		F
#3		F
#4		F
#5		F
#6		T
#10		F

import sys

#introducimos un numero
numero = sys.argv[1]

#iniciamos un contador de divisores
i=1

#suma de divisores
suma_perfecta = 0

#bucle para ir mirando divisores
while i < int(numero):
	#si es divisor, lo sumamos
	if int(numero) % i == 0:
		print(i)
		suma_perfecta = suma_perfecta + i
	#miramos el siguiente
	i = i + 1

#Mostramos resultados
if suma_perfecta == int(numero):
	print("True")
else:
	print("False")


		
