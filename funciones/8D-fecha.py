# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : dada una fecha valida, decir dia mes y año

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import sys


#leemos argumento
data = sys.argv[1]

#definimos la funcion fecha valida
def es_fecha(data):
	'''
	e: cadena
	s: enteros
	'''
	dia = int(data[0] + data[1])
	mes = int(data[3] + data[4])
	año = int(data[6] + data[7] + data[8] + data[9])
	datos = [dia, mes, año]
	return datos

#llamamos a la funcion	
print(es_fecha(data))
