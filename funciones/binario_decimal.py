# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : codificar de binari a text ASCII

#Especificacions d'entrada i joc de proves: EE: cadena binaria de byte
#01000110 		  C
#0100010101000111 EF


#funcion para pasar de binario a decimal
def bin_to_deci(num):
	'''
	input: cadena binaria de 8
	output: int
	'''
	transformacion=0
	suma = 0
	exponente = len(num)-1
	#bucle para cada numerito pasarlo a decimal
	for i in num:
		transformacion = 2**exponente * int(i)
		suma = suma + transformacion
		exponente = exponente -1
		
	return suma
	


#Funcion para pasar de binari a text ASCII
def binary_to_text(cadena):
	'''
	i:str
	o:str
	'''
	print(cadena)
	inicio = 0
	final = 8
	ascii_text = ''
	letra= ''
	#Para cada octet
	#Bucle
	for i in range(0, len(cadena)//8):
			#Retallar
			octeto = cadena[inicio:final]
			#Passem a decimal
			decimal = bin_to_deci(octeto)
			#Passar a caracter
			letra = chr(bin_to_deci(octeto))
			#Guardem char
			ascii_text = ascii_text + letra
			#preparar siguiente octeto [0:8][8:16]...
			inicio=final 
			final=final + 8
			
	return ascii_text

#Programa
entrada = input()
print(binary_to_text(entrada))
