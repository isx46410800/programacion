# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : En aquest programa introduïm el concepte de control d'errors

#Especificacions d'entrada i joc de proves: EE: un numero entero positivo

#Joc de proves
#Entrada 	Sortida ()
#5		120
#6		720
#7		5040

#definimos la funcion con un argumento
def factorial_numero(numero):
	factorial=1
	while numero>0:
		factorial = factorial * numero
		numero = numero - 1
	return factorial #aqui nos retorna un valor para el resultado de la funcion factorial_numero

#mostramos resultados pasando el argumento a la funcion definida
resultado = factorial_numero(5)
print(resultado)
resultado = factorial_numero(6)
print(resultado)
resultado = factorial_numero(7)
print(resultado)
