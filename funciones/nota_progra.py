# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : calculo nota progra

#Especificacions d'entrada i joc de proves: EE: floats


#definimos una funcion para calcular las notas pasando las notas a la funcion
def nota_programacion(n1,n2,n3,n4,n5,n6):
	'''
	e: 6 notas floats
	s: 1 float o 1 entero
	'''
	#la nota 1 y 6 valen 50%, por eso valen como una
	menos_impor = (n1+n6) / 2
	#calculo de la nota
	return int((menos_impor + n2 + n3 + n4 + n5) / 5)

#resultado llamando a la funcion
print(nota_programacion(7.5, 8.75, 5.5, 4.25, 2.75, 0.5))
	

