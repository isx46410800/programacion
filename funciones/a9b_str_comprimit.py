# !/usr/bin/python3
# -*-coding: utf-8-*-

# JORDI QUIRÓS BERBEL
# isx48172244
# 13/02/19
# versió 1.0


#ENUNCIAT: Cal fer un programa que rebi una cadena per l'entrada estàndard i en tregui per la sortida una versió comprimida segons el procediment que s'explica a continuació.

#ESPECIFICACIONS D'ENTRADA:

#JOC DE PROVES:

#               input       |        output
#       --------------------+-----------------------
#                           |
#                           |
#                           |
#                           |
#                           |
#                           |

#JOC DE PROVES ERRORS

#               input       |        output
#       --------------------+-----------------------
#                           |
#                           |
#                           |
#                           |
#                           |
#                           |


#-------------------------------------------------------------------

#REFINAMENT i CODI:

#IMPORTS
#CONSTANTS
#FUNCIONS
def comprimir(a):
    '''
    Given a str, replace the repeated chars depending on their numbers
    INPUT: str
    OUTPUT: str
    '''
    i = 0
    char_anterior = ''
    cadena_nova = ''
    for char in a:

        if char != char_anterior:
            if i <= 3:
                cadena_nova += char_anterior * i
            else:
                cadena_nova += "@"+str(i)+"@"+char_anterior
            i = 1

        else:

            i += 1

        char_anterior = char

    if i <= 3:
        cadena_nova += char_anterior * i
    else:
        cadena_nova += "@"+str(i)+"@"+char_anterior

    return cadena_nova

print(comprimir("aabbbbccd"))
