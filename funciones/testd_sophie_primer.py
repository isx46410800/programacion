# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor
#isx
#data 
#versió


#descripció: Programa que fa la mitjana de les notes
#especificacions d'entrada: un nombre enter positiu.
#Joc de proves
#--------------------------------------------------------
#	Entrada					Sortida
#--------------------------------------------------------
#	  2						  True
#	  3						  True
#	  7						  False
#	  4					 	  False


def es_primer(n):
	'''docstring'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 

def es_sophie_germain_primer(a):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	Input: Int
	Output: Bool
	'''
	#Comprovem que el número actual és un nombre primer
	if es_primer(a):
		#Si és primer, comprovar que el seu número segur també és primer
		safenum = 2 * a + 1
		return es_primer(safenum)
	return False		

print(es_sophie_germain_primer(2))
print(es_sophie_germain_primer(3))
print(es_sophie_germain_primer(7))
print(es_sophie_germain_primer(4))
