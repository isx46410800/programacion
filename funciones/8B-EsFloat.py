# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció 6- si es float o no


import sys

#introducimos el numero
cadena = sys.argv[1]


#funcion es float
#definimos la funcion es float
def es_float(cadena):
	'''
	e: cadena
	s: booleano
	'''
	flotante = True
	contador_puntos = 0
	contador_digitos = 0
	#quitamos signo si hace falta
	if cadena[0] in '+-':
		cadena = cadena[1:]
	
	#miramos cada caracter
	for c in cadena:
		#Si es un punto
		if c == '.':
			contador_puntos = contador_puntos + 1
		#Si es un digito
		elif c.isdigit():
			contador_digitos = contador_digitos + 1
		#es otra cosa
		else:
			flotante = False

	#si hay mas de un punto
	if contador_puntos>1:
		flotante = False
	#si hay menos de un digito
	if contador_digitos<1:
		flotante = False
	#Nos retorna el valor final de la variable
	return flotante

#resultado al llamar la funcion
print(es_float(cadena))


