#!/usr/bin/python
# -*-coding: utf-8-*-
#Sergi Iserte
#isx39443774
#15/02/2019
#versió

def error_longitud(cadena_binaria):
	'''
	Funció que comprova si una cadena binaria té la longitud adequada
	Entrada: str
	Sortida: str
	'''
	
	error_longitud = False
	#Comprovem si té la llargaria correcte
	if len(cadena_binaria) % 8 != 0:
		error_longitud = True
	
	return error_longitud
	
def error_nombre(cadena_binaria):
	'''
	Funció que comprova si els caracters d'una cadena binaria són correctes
	Entrada: str
	Sortida: str
	'''
	error_nombre = False
	#Comprovem si els caracters corresponen a una cadena binaria
	for car in cadena_binaria:
		if car != '0' and car != '1':
			error_nombre = True
		
	return error_nombre
	

def bin_to_dec(byte):
	'''
	Funció que realitza la conversió de binari a decimal
	Entrada: str
	Sortida: int
	'''
	#Establim variables
	i = 2**(len(byte)-1)
	decimal = 0
	#Realitzem la conversio
	for car in byte:
		if car == '1':
			decimal = decimal + i
		i = i//2
		
	return decimal

def binariAText(cad_bin):
	'''
	Funció que realitza la conversió de binari a caracter, 
	segons el seu valor decimal en la taula ASCII
	Entrada: cadena
	Sortida: cadena
	'''
	#Establim variables
	cad_bin_modificable = cad_bin
	i=0
	text=''
	#Consultem el nombre de bytes que tenim
	n_bytes = len(cad_bin)/8

	#Per a cada byte realitzem la seva conversio al seu caracter
	while n_bytes > i:
		#Funcions per fer la conversio de bin a text
		caracter = chr(bin_to_dec(cad_bin_modificable[:8]))
		text = text + caracter
		cad_bin_modificable = cad_bin_modificable[8:]
		i += 1
	return text
	

if __name__ == '__main__':
	if False:
		print(bin_to_dec('01000000'))
		print(bin_to_dec('01000001'))
		print(bin_to_dec('11111111'))
		print(bin_to_dec('00000001'))
	if False:
		print(binariAText('0100000101000010'))
		print(binariAText('01000100'))
		print(binariAText('01110010011001010110001101110101011011000110110001101111001000000110110000100111011001010111100001100001011011010110010101101110001000000110000100100000011011000110010101110011001000000011000100110011001110100011000000110000001000000110010101101110001000000111000001110101011011100111010000101110')) # recullo l'examen a les 13:00 en punt.
	if False:
		print(error_longitud('01000100'))
		print(error_longitud('010001'))
		print(error_longitud('0100000100'))
	if False:
		print(error_nombre('35428654'))
		print(error_nombre('10000000'))
		print(error_nombre('22222222'))


