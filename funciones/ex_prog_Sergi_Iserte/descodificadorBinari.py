#!/usr/bin/python
# -*-coding: utf-8-*-
#Sergi Iserte
#isx39443774
#15/02/2019
#versió

import funcions_binari

#E.E: cadena binaria, en str
#Joc de proves de control d'errors
#	e1					|	s1
#	1111000					ERROR: longitud de la cadena binaria incorrecte, ha de ser multiple de 8
#	21110000				ERROR: la cadena binaria ha d'estar en nombres binaris, nombres 0 o 1
#	AS329882				ERROR: la cadena binaria ha d'estar en nombres binaris, nombres 0 o 1


#programa principal       

cadena_binaria = input()

if funcions_binari.error_longitud(cadena_binaria):
	print("\nERROR: longitud de la cadena binaria incorrecte, ha de ser multiple de 8\n",\
	"USAGE: python3 prog.py [0-1]{8}...\n")
	exit(0)
	
if funcions_binari.error_nombre(cadena_binaria):
	print("\nERROR: la cadena binaria ha d'estar en nombres binaris, nombres 0 o 1\n",\
	"USAGE: python3 prog.py [0-1]{8}...\n")
	exit(0)


print(funcions_binari.binariAText(cadena_binaria))



