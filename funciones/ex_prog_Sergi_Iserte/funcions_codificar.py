#!/usr/bin/python
# -*-coding: utf-8-*-
#Sergi Iserte
#isx39443774
#15/02/2019
#versió

def dec_to_bin(num):
	'''
	Funció que realitza la conversió d'un nombre en base 10 a base 2
	Entrada: int, base 10 
	Sortida: str, base 2
	'''
	base2 = ''
	
	#convertim dec to bin
	while num > 0:
		base2 = str(num % 2) + base2
		num = num // 2
	
	#Agregem 0 fins a convertirlo en un byte
	while len(base2) < 8:
		base2 = '0' + base2 
	
	return base2 

def textABinari(text):
	'''
	Funció que realitza la codificació d'un text a binari
	Entrada: str
	Sortida: str
	'''
	binari=''
	#Realitzem la codificació
	for car in text:
		binari = binari + (dec_to_bin(ord(car)))
		
	return binari
		

	
	
if __name__ == '__main__':
	if False:
		print(dec_to_bin(198))
		print(dec_to_bin(10))
		print(dec_to_bin(225))
		print(dec_to_bin(1))
	if False:
		print(textABinari('AB'))
		print(textABinari('KAKA'))
		print(textABinari('XD'))
