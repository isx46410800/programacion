# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 15/02/2019
#versió 1

#descripció : convertir una cadena binaria a text

#Especificacions d'entrada i joc de proves: EE: cadena de 0 i 1

#JOC DE PROVES ERRORS
#E 		 	S
#01000000 10000000	F
#PATATA			F
#.			F
#0100000010000000	T
#+10000000		F
#-10000000		F
#01000000.10000000	F
#02000000		F


#Exemple:
#0100000101000010 = AB

#defimos la funcion
def binariAText(cadenaBinaria):
	'''
	Entrada: string
	Sortida: String
	'''
	#contamos cuantos digitos hay
	cantidad = len(cadenaBinaria)
	#seperamos de 8 en 8

	#por cada grupo de 8
		#invertimos la cadena
		#calculamos el numero segun la posicion
		#convertimos el numero en char(num
	#return char(num)

#programa principal 
      
#entrada
cadena = input()

#ponemos la cadena del reves
cadena_inversa = cadena[::-1]


print(cadena_inversa)



