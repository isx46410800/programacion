# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1

import sys

entrada = sys.argv[1]

#quitamos signo
if entrada[0] in '+-':
	entrada = entrada[1:]

#definimos la funcion es entero y miramos si toda la cadena es digito
def es_enter(entrada):
	return entrada.isdigit()

#mostramos resultado true o false
print(es_enter(entrada))


