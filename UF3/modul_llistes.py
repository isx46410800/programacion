#nom autor diego sanchez piedra
# !/usr/bin/python
# -*-coding: utf-8-*-

#nom autor diego sanchez piedra
#isx2031424
#data 01 /03 /2019
#versió 1

#ATENCIÓ: Aquest mòdul no funcionava correctament. Tenia els següents problemes:
    #El test_driver no funcionava
    #la prova de l'histograma tampoc
    #La funció histograma tampoc
    
#NO PODEU PROGRAMA SENSE JOCS DE PROVES!!!!!!

import sys

#maxim
def n_max(llista):
    '''
    function that gives the greatest number of the list NON EMPTY
    input=llista d'enters NO BUIDA
    ouput=int
    '''
    maxim=llista[0]

    for i in llista:
        if i > maxim:
            maxim=i
    
    return maxim
    
#llista_str_to_int
def llista_str_to_int(llista):
    '''
    function that converts a str list to an int list
    input=llista de str
    ouput=llista de int
    '''
    llista_int=[]
    for e in llista:
        llista_int.append(int(e))
    
    return llista_int

#histograma
def histograma(l_filtres,l_freq):
    '''
    input:llista strings,llista int(positiu)
    output:void
    ''' 
    character='*'
    for i in range(len(l_freq)):
        if l_freq[i] > 0:
            print("%-15s %s" % (l_filtres[i],character*l_freq[i]))
    return

#compta_freqs
def freq_cont(frase,filtres):
    '''
    function that counts how many times a determined letter appears in a list
    input=str
    ouput=lista frecuencia vocals
    ''' 

    freq=[]
    #afegim 0 com a caracters i hagin especificats per buscar
    freq=[0]*len(filtres)
    #recorrem el text 
    for c in frase:
        #si coincideix amb els caracter espeficiats sumem 1 a la seva posicio
        if c in filtres:
            freq[cerca_sequencial(filtres,c)] += 1
        
    return freq

def cerca_sequencial (llista , element):
    '''
    Retorna la 1a posicio d'un element a una llista. (-1 si no hi es)
    retorna la ultima aparició
    Entrada: list element , element
    Sortida: int
    '''
    
    for i in range(0,len(llista)):
                if element == llista[i]:
                    return i
    
    return -1
 
def freq_cont_list(llista):
    '''
    funcio que conpta la frequencia de les paraules en una llistad doana
    input= str list
    ouput= str list and in list
    '''
    llista_paraules=[]
    llista_freq=[]
    for element in llista:
        if element in llista_paraules:

            llista_freq[cerca_sequencial(llista_paraules,element)] += 1

        else:
            llista_paraules.append(element)
            llista_freq.append(1)
    return llista_freq

#-------test driver-------------------------
if __name__ == '__main__' :
    if True:
        print('-----------llista_str_to_int---------')
        print(llista_str_to_int(['4','3','2','1']))
    if True:
        print('---------- n_max---------------------')
        print(n_max([1,2,3,4,5]))
    if True:
        print('--------------histograma-------------')
        print(histograma(['a','b'],[2,3]))
    if True:
        print('----------------contador de frequencies--------------')
        print(freq_cont('hola hola que tal tal','hola'))
    if True:
        print('-----------------normalitzar text----------------')
        print(normalitzar_text('hola, que tal?'))
    if True:
        print(get_filter('hola hola hola diego diego diego'))
