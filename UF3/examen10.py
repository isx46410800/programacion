#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#CORRECION EXAMEN
#------------------------------------------
import sys
import modulos_llistas
#1. Llegim la quantitat que volem tenir en  canvi
#2. Creem la llista de les freqüències, amb valors 0
#3. Cridem a la funció torna_canvi, que ens desglossarà la quantitat
#segons l’algoritme del mínim nombre de bitllets (el normal)
#4. Fem l’histograma horitzontal utilitzant la nostra funció del mòdul

BITLLETS=[1, 10, 50, 500, 200, 100, 20, 5, 2]
#es pot adaptar a tots els països i quantitats

def torna_canvi(cant,l_billetes):
	'''
	input: int de cantidad, llista int de billetes
	ouput: diccionario {500:4}
	'''
	#ordenar bitllets
	l_billetes.sort(reverse=True)
	d={}
	#repartir en el diccionario
	for b in l_billetes:
		d[b]= cant//b
		cant=cant%b
	return d
	
def histograma(llista_items):
	'''
	input: llista de llistes[(a:3), (b:4..)]
	sortida: res
	'''
	character='*'
	for k in llista_items:
		#print(k, k[0],k[1])
		print(f"{k[0]:-4}\t{character*k[1]}")
	return

#programa
cant=int(input())

db=torna_canvi(cant, BITLLETS)

l=list(db.items())

print(l)
#histograma ordenat per valor
l.sort(reverse=True)
print(l)

histograma(l)

#histograma per quantitat
# l_sort(key=k_segon_camp)

# histograma(l)


#-------test driver-------------------------
if __name__ == '__main__' :
    if True:
        print('-----DESGLOSE------')
        print(torna_canvi(2141,[1, 10, 50, 500, 200, 100, 20, 5, 2])) #{500: 4, 200: 0, 100: 1, 50: 0, 20: 2, 10: 0, 5: 0, 2: 0, 1: 1}
