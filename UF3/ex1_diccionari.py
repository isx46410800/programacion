#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#DICCIONARIS
#------------------------------------------
import funcions_dates


# 1.  Fes una funció que donat un diccionari i un valor, digui si aquest valor està o no en el diccionari.
def diccionari_valor(diccionario, valor):
	'''
	input: un diccionario y un valor
	output: booleano
	'''
	for k in diccionario:
		if diccionario[k] == valor:
			return True
			
	return False
	
	
def diccionari_valor2(diccionario, valor):
	'''
	input: un diccionario y un valor
	output: booleano
	'''
			
	return valor in diccionario.values()
	

#2. Fes una funció que retorni una llista amb totes les claus d'un diccionari.	
def diccionari_claus(diccionario):
	'''
	input: un diccionari
	output: una llista de claus
	'''
	l_claus=[]
	
	for k in diccionario:
		l_claus.append(k)
	
	return l_claus
	
#4.  Fes una funció que dibuixi un histograma horitzontal d'un diccionari amb els valors numèrics.	
def histograma(diccionario):
	'''
	input: diccionari
	output: res, dibuixa histograma
	'''
	character='*'
	for k in diccionario:
		print(f"{k:-4}{character*diccionario[k]}")
	return

#5. Fes una funció que tradueixi una data en format numèric de la següent manera: 01/01/2010  --> 1 de gener de 2010.
def mes_nom(mes):
	'''
	input: mes format numero
	output: mes format nom
	'''
	meses=['invalid','gener','febrer','març','abril','maig','juny','juliol','agost','septembre','octubre','novembre', 'desembre']
	
	mes_nombre = meses[mes]
	
	return mes_nombre
	
def data_to_text(data):
	'''
	input: data format dd/mm/aaaa
	ouput: data format 1 de mes de any
	'''
	meses={0:'invalid',1:'gener',2:'febrer',3:'març',4:'abril',5:'maig',6:'juny',7:'juliol',8:'agost',9:'septembre',10:'octubre',11:'novembre', 12:'desembre'}
	
	dia = funcions_dates.get_day(data)
	mes = funcions_dates.get_month(data)
	anyo = funcions_dates.get_year(data)
	
	#print(dia, 'de', meses[mes], 'de', anyo)
	#return str(dia) + ' de ' + str(meses[mes]) + ' de ' + str(anyo)
	return f"{dia} de {meses[mes]} de {anyo}"

#5. Fes una funció que tradueixi una data en format numèric de la següent manera: 1 de gener de 2010 --> 01/01/2010.
def text_to_data(text):
	'''
	input: una fecha en formato string
	output: una fecha en formato dd/mm/aaaa
	'''
	meses={'invalid':0,'gener':1,'febrer':2,'març':3,'abril':4,'maig':5,'juny':6,'juliol':7,'agost':8,'septembre':9,'octubre':10,'novembre':11,'desembre':12}
	
	text=text.split()
	dia=int(text[0])
	anyo=int(text[4])
	mes=text[2]
	mes_data=meses[mes]
	
	#print(text,dia,anyo,mes_data)
	
	return funcions_dates.set_date(dia,mes_data,anyo)

# 7.  Dissenya un programa que rebi per paràmetre una frase qualsevol (per entrada standard) i compti quantes vegades surt cada una de les vocals. Dissenya'n un altre que per cada caràcter donat compti les vegades que surt. 
# Presenta els resultats de les següents maneres:
# • Amb un histograma
# • Mostrant el diccionari 'maco':
# clau: a	valor: 8
# clau: u	valor: 3
	
#leer entrada	
#entrada=input()


#contar vocales
def frecuencias(secuencia, elem_a_contar):
	'''
	input: una llista/string i llista/string d'elements a buscar
	output: diccionari de freqs
	'''
	#crear el diccionario con los ceros
	diccionario=dic_a_cero(elem_a_contar)
	
	#contar
	for element in secuencia:
		if e in diccionario:
			d[element]+=1
			
	return diccionario
	
	

def dic_a_cero(secuencia):
	'''
	input: cadena o llista
	output: diccionari amb valors 0
	'''
	diccionario={}
	
	for element in secuencia:
		diccionario[element]=0
		
	return diccionario
	
def diccionario_maco(dic):
	'''
	input: dic
	output: res
	'''
	for k in dic:
		print(f'{k}\t{d[k]}')
	
		
#hacer histograma

	
	
	
	
	
	
#-------test driver-------------------------
if __name__ == '__main__' :
    if False:
        print('-----------VALOR EXISTE O NO DICCIONARIO---------')
        print('-----------METODO 1---------')
        print(diccionari_valor({500:40,200:5},5)) #TRUE
        print(diccionari_valor({500:40,200:5},1)) #FALSE
        
        print('-----------METODO 2---------')
        print(diccionari_valor2({500:40,200:5},2)) #FALSE
        print(diccionari_valor2({500:40,200:5},5)) #TRUE
        
        print('-----------LISTA DE CLAVES---------')
        print(diccionari_claus({500:40,200:5})) #[500,200]
        print(diccionari_claus({1:4,2:5,3:1})) #[1,2,3]
        
        print('-----HISTOGRAMA-----------')
        histograma({1:4,2:5,3:3})
        
        print('---DATA TO TEXT-')
        print(mes_nom(1)) #gener
        print(data_to_text('01/01/2010')) #1 de gener de 2010
        print(data_to_text('31/03/2019')) #31 de març de 2010
        
        print('---TEXT TO DATA---')
        print(text_to_data('1 de gener de 2010')) #01/01/2010
        print(text_to_data('31 de març de 2019')) #31/03/2019
        
	if True:
		print(dic_a_cero
        
