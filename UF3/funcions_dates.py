# !/usr/bin/python
# -*-coding: utf-8-*-

#Mòdul de funcions per dates
#Autor: Jordi Quirós 2019



import datetime

def correct_format(a):

    '''
    Given a str that represents a date,
    it verifies that is a correct date (DD/MM/YYYY)
    INPUT = str that represents a date
    OUTPUT = Bool
    '''

    if not (len(a) == 10):

        return False

    num = a[0:2] + a[3:5] + a[6:]
    are_slash = a[2] == "/" and a[5] == "/"

    if not (num.isdigit() and are_slash):

        return False

    return True

def leap_year(a):

    '''
    Given a int that represents a year, says if its a leap year or not
    INPUT= int > 0 (year)
    OUTPUT = BOOL
    '''

    return (a % 400 == 0 or (a % 4 == 0 and not a % 100 == 0))



def get_day(a):

    '''
    Given a str that represents a correct froamt date (DD/MM/YYYY),
    extracts de day of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    day = int(a[0:2])

    return day

def get_month(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de month of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    month = int(a[3:5])

    return month

def get_year(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de year of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    year = int(a[6:10])

    return year


#He modificat l'entrada perquè em sembla més adeqüat llegir dos int
# def last_day_of_month(month, year):

    # '''
    # Says the last day of that month.
    # INPUT = int (correct month), int(year)
    # OUTPUT= int
    # '''

   

    # last_days = [0,31,28,31,30,31,30,31,31,30,31,30,31]

    # # IF ITS FEBRUARY AND A LEAP-YEAR

    # if month == 2 and leap_year(year):

        # last_days[2] = 29

    # return last_days[month]

def last_day_of_month(a):

    '''
    Given a str that represents a correct date, says the last day of that month.
    INPUT = str (correct date)
    OUTPUT= int
    '''

    month = get_month(a)
    year = get_year(a)

    last_days = [0,31,28,31,30,31,30,31,31,30,31,30,31]

    # IF ITS FEBRUARY AND A LEAP-YEAR

    if month == 2 and leap_year(year):

        last_days[2] = 29

    return last_days[month]

def valid_date(a):

    '''
    Given a str that represents date, says if the input string is a correct date.
    INPUT = str in the form of XX/XX/XXXX where X is an int
    OUTPUT = Bool
    '''

    if not correct_format(a):

        return False

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)


    if year < 0:

        return False

    if not (month >= 1 and month <= 12):

        return False

    if not (day >= 1 and day <= last_day_of_month(month, year)):

        return False

    return True

def set_date(d,m,y):

    '''
    Given 3 ints > 0 that represent a day, month and year respectively,
    creates a str in the form of DD/MM/YYYY
    INPUT = 3 ints > 0
    OUTPUT = 1 str (DD/MM/YYYY)
    '''

    return f"{d:02}/{m:02}/{y:04}"
    


def today():

    '''
    Function that returns the current date like DD/MM/YYYY
    INPUT = None
    OUTPUT = str (correct date DD/MM/YYYY)
    '''
    t = datetime.datetime.now()
    return set_date(t.day,t.month,t.year)

def next_day(a):

    '''
    Given a str that represents a corrent date,
    it returns the following day.
    INPUT = str (correct date DD/MM/YYYY)
    OUTPUT = str (correct date DD/MM/YYYY)
    '''

    # Extract day, month and year

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)

    # Increase the day by 1

    day += 1

    # Check if the day is a correct date and make the necessary changes

    # If the day is not corect

    if day > last_day_of_month(month, year):

        day = 1
        month += 1

    # If the month is not correct

    if month > 12:

        month = 1
        year += 1

    return set_date(day,month,year)

def date_comparator(date1, date2):

    '''
    Function that given 2 correct dates (in the form of DD/MM/YYYY),
    returns an int (-1 or 0 or 1) depending on whether the first one
    is an earlier date (-1), it's the same date (0)
    or the second one is a later date (1).
    '''

    status=1

    d1_day= get_day(date1)
    d1_month= get_month(date1)
    d1_year= get_year(date1)

    d2_day= get_day(date2)
    d2_month= get_month(date2)
    d2_year= get_year(date2)

    if d1_year < d2_year:

        status = -1

    elif d1_year == d2_year:

        if d1_month < d2_month:

            status = -1

        elif d1_month == d2_month:

            if d1_day < d2_day:

                status = -1

            elif d1_day == d2_day:

                status = 0

    return status
 
#definimos la funcion bombolla
def bombolla(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista d'enters
	Sortida: llista d'enters
	'''
	#bucle para el rango de la lista de enteros
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		#miramos las posicion para ordenarlo de menor a mayor
		for j in range(0, final): 
			if (llista[j] > llista[j+1]):
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	#return
	return llista
	    
#definimos la funcion bombolla dates
def bombolla_dates(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista de dates
	Sortida: llista de datas ordenada
	'''
	#bucle para el rango de la lista de enteros
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		#miramos y comparamos para ordenarlo de menor a mayor
		for j in range(0, final): 
			if date_comparator(llista[j],llista[j+1]) == 1:
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	return llista
	
def data_a_num(data):
	
	'''
	entrada: str data format correcta
	sortida: int
	'''
	
	llista_nums = []
	#obtenim el mes i els dias i l'any
	dia = funcions_dates.get_day(data)
	mes = funcions_dates.get_month(data)
	y = funcions_dates.get_year(data)
	
	# establim la data per a calcular els mesos que han pasat
	mes = mes - 1
	data = funcions_dates.set_date(dia,mes,y)
	count = 0	
	
	# calculem els dias que han anat pasant
	while mes > 0:
		
		ultim_dia = funcions_dates.last_day_of_month(data)
		count = count + ultim_dia
		mes = mes - 1
		data = funcions_dates.set_date(dia,mes,y)
	
	count = count + dia	
	
	# obtenim la llista amb la data pasada a int
		
	return count

if __name__ == '__main__':

    #TEST DRIVES

    if False:

        print('correct format')
        print(correct_format('3')) #False
        print(correct_format('08/061/997')) #False
        print(correct_format('08/06/1997')) #True

    if False:

        print('leap_year')
        print(leap_year(2008)) #True
        print(leap_year(2007)) #False

    if False:

        print('get_day')
        print(get_day('08/06/1997')) # 8
        print(get_day('99/06/1997')) # 99

    if True:

        print('last_day_of_month')
        print(last_day_of_month(6, 1997)) #30
        print(last_day_of_month(2, 1997)) #28
        print(last_day_of_month(2, 2000)) #31
        print(last_day_of_month(2, 1900)) #29

    if False:

        #EL JOC DE PROVES DE VALID DATE ÉS INDUFICIENT
        print('valid_date')
        print(valid_date('08/06/1997')) #True
        print(valid_date('08/06/20188')) #False
        print(valid_date('33/06/1997')) #False
        print(valid_date('08/14/1997')) #False
        print(valid_date('29/02/2000')) #True

    if False:

        print('set_date')
        print(set_date(8,6,1997))
        print(set_date(1,1,1))
        print(set_date(99,99,9999))

    if False:

        print('next_day')
        print(next_day('01/01/2000')) # 02/...
        print(next_day('28/02/2000')) # 29/...
        print(next_day('29/02/2000')) # 01/03...
        print(next_day('31/01/2000')) # 01/02/...
        print(next_day('31/12/2000')) # 01/01/2001

    if False:

        print('date_comparator')
        print(date_comparator('01/01/2019','08/06/2019')) # -1
        print(date_comparator('31/12/2019','08/06/2019')) # 1
        print(date_comparator('08/06/1997','08/06/1997')) # 0
        
	# if True:
	   # print(data_a_num('05/03/1997'))#64
	   # print(data_a_num('08/05/1999'))#128
	   # print(data_a_num('15/11/2005'))#319
    #bombolla fechas
    # if True:
		# print(bombolla([1,5,2])) #[1,2,5]
		# print(bombolla([10,5])) #[5,10]
		# print(bombolla([11,-5,2,7])) #[-5,2,7,11]
		# print(bombolla([10,50,20,7])) #[7,10,20,50]
        
	#if True:
		#print(bombolla_dates(['08/03/2017','09/02/2017','09/02/2016'])) #['09/02/2016', '09/02/2017', '08/03/2017']
		#print(bombolla_dates(['09/02/2017','08/03/2017','09/02/2017','09/02/2016','08/02/2017','09/02/2017','08/02/2017'])) #['09/02/2016', '08/02/2017', '08/02/2017', '09/02/2017', '09/02/2017', '09/02/2017', '08/03/2017']

        
    
