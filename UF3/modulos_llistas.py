#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#FUNCION PARA CALCULO DE UN NUMERO MAX EN UNA LISTA
def n_max(llista):
	'''
	input=llista d'enters NO BUIDA
	ouput=int
	'''
	maxim=llista[0]

	for i in llista:
		if i > maxim:
			maxim=i
	
	return maxim
	
#FUNCION PARA BUSCAR LA POSICION EN UNA LISTA
def buscar_posicion(llista_palabras, palabra):
	'''
	entrada: lista de strings y un string a buscar
	output: un int de posicion
	'''
	contador=0
	posicion=0
	
	for char in llista_palabras:
		if char==palabra:
			posicion=contador
			return posicion
		contador = contador + 1
	return -1
	
#FUNCION PARA CONTAR LA FRECUENCIA DE CIERTAS PALABRAS EN UNA LISTA
def contador_segun_palabras(palabras, llista):
	'''
	entrada: pasamos una lista de str
	sortida: lista de freqs
	'''	
	contador_freqs=[0]*len(palabras)
	
	for paraula in llista:
		if paraula in palabras:
			posicion=buscar_posicion(palabras,paraula)
			contador_freqs[posicion] +=1
				
	return contador_freqs


#FUNCION PARA CONTAR LA FRECUENCIA DE TODAS LAS PALABRAS EN UNA LISTA
	
def contar_palabras(cadena):
	'''
	input: cadena de strings
	output: dos listas de palabras sin repetir y lista de freqs
	'''
	llista_texto=cadena.split()
	#print(llista_texto)
	palabras=[]
	cont_freq=[]
	
	for paraula in llista_texto:
		if paraula in palabras:
			posicion=buscar_posicion(palabras,paraula)
			cont_freq[posicion] +=1
		else:
			palabras.append(paraula)
			cont_freq.append(1)
				
	return palabras,cont_freq
	
#FUNCION PARA HACER UN HISTOGRAMA SEGUN FRECUENCIAS
def histograma(palabras, cont_freq):
	'''
	dibuixa un histograma horitzontal
	entrada: llista de str, llista enteros positius
	sortida: none
	'''		
	CARACTER='*'
	for pos in range(0, len(palabras)):
		print(f"{palabras[pos]:10}{CARACTER*cont_freq[pos]}")
	
	return	

##FUNCION PARA NORMALIZAR UN TEXTO QUITANDO SIGNOS
def normalizar_texto(texto):
    '''
    funcio que normalitza un text treient els signes
    input=str
    ouput=str
    '''
    nova_cadena=''
    llista_signes=['.',':',',',';','!','?']
    for c in texto:
        if c in llista_signes:
            nova_cadena=nova_cadena + ' '
        else:
            nova_cadena = nova_cadena + c 
    return nova_cadena
    
##FUNCION DIFERENCIA DE LISTAS
def diferencia(llista1,llista2):
	'''
		entrada: llista
		sortida: llista nova
	
	'''
	
	llista_buida=[]
	
	for element in llista1:
		
		if element not in llista2:
			
			llista_buida.append(element)
			
	return llista_buida
	

##FUNCION PARA INTERSECCION DE LISTAS
def interseccio(llista1,llista2):
	'''
		entrada: llista
		sortida: llista
	'''
	
	intersec=[]
	
	for element in llista1:
		if element in llista2:
			intersec.append(element)
			
	return intersec
	

##FUNCION PARA UNION DE LISTAS	
def unio(llista1,llista2):
	'''
	   entrada: llista 
	   sortida: llista
	'''
	
	unio=llista1
	
	for element in llista2:
		
		if element not in unio:
			unio.append(element)
	return unio

def str_to_int(llista):
	'''
	entrada: llista de str
	sortida: llista de int
	'''
	#creamos lista vacia de enteros
	llista_int=[]
	
	#vamos pasando cada elemento de la lista de strings y lo añadimos a la lista
	for element in llista:
		llista_int.append(int(element))
		#llista_int = llista_int + int(element) otra opcion
		
	return llista_int
	
# def frequencies(llista_text, llista_final):
	# '''
	# entrada: llista_text(llista de sts amb les paraules del text
			 # llista_paraules(llista que tindra les paraules a comptar
	# sortida: llista de freqs
	# '''
	# text=input()
	# text_l=text.split()
	# l_p_totals=[]
	# freqs=frequencies(text_l, l_p_totals)
	    	
#TEST DRIVER
if __name__ == '__main__':
	if True:
		print("BUSCAR POSICION")
		print(buscar_posicion(['hola', 'miguel', 'adeu'], 'adeu')) #2
		print(buscar_posicion(['hola', 'miguel', 'adeu'], 'aeu')) #-1
		print(buscar_posicion(['hola', 'miguel', 'adeu', 'adeu'], 'adeu')) #2
		print(buscar_posicion(['hola', 'miguel'], 'miguel')) #1
		
		print("CONTAR LAS FRECUENCIAS")
		print(contar_palabras("Me llamo miguel")) #[0,1]
		print(contar_palabras("Me llamo miguel miguel hola")) #[1,2]
		
		print("HISTOGRAMAS")
		histograma(['miguel','hola'], [4,2]) #los histogramas se ponen sin print para que no salga 'none' en los return
		print("")
		histograma('aeiou', [2,2,2,2,2])
		
		print('NORMALIZAR TEXTO QUITANDO SIGNOS')
		print(normalizar_texto('hola, que tal?')) #hola que tal
		
		print('CALCULO NUMERO MAXIMO')
		print(n_max([1,2,3,4,5]))#5
		
		print('CONTAR PALABRAS')
		print(contar_palabras('hola hola que tal miguel'))# [hola, que, tal, miguel], [2,1,1,1]
		
		print('CONTAR PALABRAS SEGUN FILTRO')
		print(contador_segun_palabras(['hola', 'miguel'], ['hola', 'miguel', 'hola', 'miguel'])) #[2,2]
		
		print("LISTAS DIFERENCIA")
		print(diferencia(['hola','casa'],['hola','quimet']))
		print(diferencia(['hola'],['hola']))
		print(diferencia(['hola'],['adeu']))

		print("LISTAS INTERSECCION")
		print(interseccio(['hola'],['hola']))
		print(interseccio(['casa','ballo','canto'],['canto','ballo','surto']))

		print("LISTAS UNION")
		print(unio(['hola','casa','samba'],['hola','samba','cantem','pis']))
		
	if False:	
		#dar una lista de str a int
		print(str_to_int(['3','5','2']))
		print(str_to_int(['-3','-5','-2']))
