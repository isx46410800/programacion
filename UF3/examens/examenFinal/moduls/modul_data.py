#!/usr/bin/python
#-*-coding: utf-8*-
#################################################################
#NOM AUTOR:		Daniel Cano 
#ISX: 			isx53320079
#DATA: 			17/1/2018
#VERSIÓ: 		1.0
#################################################################
def valida_format_data(data):
	'''
	Funcio que valida el format d'una data
	Entrada: str en format data
	Sortida: bool
	'''
	if len(data) != 10:
		return False
	return data[:2].isdigit() and data[2] == '/' \
	and data[3:5].isdigit() and data[5] == '/' \
	and data[6:].isdigit()

def get_dia(data):
	'''
	Funcio que retorn el dia d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[:2])
	
def get_mes(data):
	'''
	Funcio que retorn el mes d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[3:5])

def get_any(data):
	'''
	Funcio que retorn el any d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[6:])

def es_any_de_traspas(anyo):
	'''
	Funcio que diu si es any de traspas
	Entrada: int
	Sortida: bool
	'''
	return (anyo % 4 == 0 and anyo % 100 != 0) or (anyo % 400 == 0) 

def ultim_dia(data):
	'''
	Funcio que retorna l'ultim dia del mes
	Entrada: str en format data
	Sortida: int
	'''
	llista_dies = [0,31,28,31,30,31,30,31,31,30,31,30,31]
	mes = get_mes(data)
	if es_any_de_traspas(get_any(data)):
		llista_dies[2] = 29
	return llista_dies[mes]
	
def data_valida(data):
	'''
	Funcio que valida una data
	Entrada: str en format data
	Sortida: bool
	'''
	#es format correcte
	if not valida_format_data(data):
		return False
	#si la'any es incorrecte
	if get_any(data) < 0:
		return False
	mes = get_mes(data)
	dia = get_dia(data)
	return (mes >= 1 and mes <= 12) and \
	(dia <= ultim_dia(data) and dia >= 1)
		

def data_a_format_correcte(dia,mes,anyo):
	'''
	funcio que segons un dia, mes y any el pasa a format data
	entrada: 3 int
	retorn:string en format data
	'''
	return "%02d/%02d/%04d" % (dia,mes,anyo)

def dia_seguent(data):
	'''
	Funcio que retorna el dia seguent
	Entrada: str en format data
	Sortida: str en format data
	'''
	dia = get_dia(data)
	mes = get_mes(data)
	anyo = get_any(data)
	
	dia = dia + 1
	if dia > ultim_dia(data):
		dia = 1 
		mes = mes + 1
		if mes > 12:
			mes = 1
			anyo = anyo + 1
	return data_a_format_correcte(dia,mes,anyo)
	
def algoritme_dia_setmana(data):
	'''
	Funcio que donada una data retorna un int de 0-6
	Entrada: str en format data
	Sortida: un int de 0-6
	'''
	#obtenim dia, mes i any
	dia	= get_dia(data)
	mes	= get_mes(data)
	anny = get_any(data)
	#calculem i
	a = (14-mes)/12
	b = anny - a
	c = mes - 2 + a*12 
	d = b/4
	e = b/100
	f = b/400
	g = (31*c)/12
	h = dia + b + d - e + f + g
	i = h % 7
	if i > 6:
		i = i % 7
	return i
	
def nom_dia_setmana(data):
	'''
	Funcio que retorn el dia de la setmana
	Entrada: un int de 0-6
	Sortida: en format data
	'''
	llista_dias=['diumenge','dilluns','dimarts',\
	'dimecres','dijous','divendres','dissabte']
	
	i = algoritme_dia_setmana(data)
	
	return llista_dias[i]

def dia_anterior(data):
	'''
	Funcio que retorna el dia anterior
	Entrada: str(data)
	Sortida: str(data)
	'''
	dia = get_dia(data)
	mes = get_mes(data)
	anny = get_any(data)	
	dia = dia - 1
	if dia == 0:
		mes = mes - 1
		if mes == 0:
			mes = 12
			anny = anny - 1
		dia = ultim_dia(data_a_format_correcte(dia,mes,anny))
	return data_a_format_correcte(dia,mes,anny)
	
def compara_datas(data1,data2):
	'''
	Funcio que compara dos datas 
	Entrada: 2 string en format data
	Sortida: 0 si son iguals
			 1 si la primera es mes gran
			 -1 si la segona es mes gran
	'''
	dia1 = get_dia(data1)
	mes1 = get_mes(data1)
	any1 = get_any(data1)
	
	dia2 = get_dia(data2)
	mes2 = get_mes(data2)
	any2 = get_any(data2)
	
	if any1 > any2:
		valor = 1
	elif any1 < any2:
		valor = -1
	else:
		if mes1 > mes2:
			valor = 1
		elif mes1 < mes2:
			valor = -1
		else:
			if dia1 > dia2:
				valor = 1
			elif dia1 < dia2:
				valor = -1
			else:
				valor = 0
				
	return valor	
		
def bombolla_data(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista de datas
	Sortida: llista de datas
	'''
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		for j in range(0, final): 
			if compara_datas(llista[j],llista[j+1]) == 1:
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	return  

#Test driver
if __name__ == "__main__":
	#valida data
	if False:
		print valida_format_data('12/12/2018')
		print valida_format_data('aa/aa/aaaa')
		print valida_format_data('12/03/0232')
	#get dia
	if False:
		print get_dia('12/12/2018')
	#get mes
	if False:
		print get_mes('12/10/2018')
	#get any
	if False:
		print get_any('12/12/2018')
	#es any de traspas
	if False:
		print es_any_de_traspas(2017)
		print es_any_de_traspas(2016)
	#ultim dia	
	if False:
		print ultim_dia('29/02/2017')
		print ultim_dia('29/02/2016')
		print ultim_dia('31/12/2016')
		print ultim_dia('32/11/2016')
	#data valida
	if False:
		print data_valida('29/02/2017')
		print data_valida('29/02/2016')
		print data_valida('31/12/2016')
		print data_valida('31/11/2016')
	#data a format correcte
	if False:
		print data_a_format_correcte(31,13,2016)
		print data_a_format_correcte(31,1,2016)
	#dia seguent
	if True:
		print dia_seguent('28/02/2016')
		print dia_seguent('28/02/2015')
		print dia_seguent('31/12/2016')
		print dia_seguent('30/05/2018')
		print dia_seguent('01/06/2018')

	#nom dia setmana
	if False:
		print nom_dia_setmana('06/06/1997')
		print nom_dia_setmana('07/06/1997')

	#dia anterior
	if False:
		print dia_anterior('01/03/2016')
		print dia_anterior('01/03/2017')
		print dia_anterior('01/01/2000')
	#compara data
	if True:
		print compara_datas('09/02/2017','08/02/2017')
		print compara_datas('08/02/2017','09/02/2017')
		print compara_datas('08/02/2017','08/02/2017')
		print compara_datas('09/02/2016','08/02/2017')
		print compara_datas('09/02/2017','08/02/2016')
		print compara_datas('09/03/2017','08/02/2017')
		print compara_datas('09/02/2017','08/03/2017')
	#bombolla datas
	if False:
		llista = ['09/02/2017','08/03/2017','09/02/2017','09/02/2016'\
		,'08/02/2017','09/02/2017','08/02/2017']
		bombolla_data(llista)
		print llista
