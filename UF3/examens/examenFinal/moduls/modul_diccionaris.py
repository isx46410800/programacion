#!/usr/bin/python
#-*- coding: utf-8 -*-

#Samuel Baena Hayas
#isx46949521
#04/04/2018
#Versió 1.0

#FUNCIONS

def crea_taula_dd(diccio,val_ord):
	'''
	Donat un diccionari de diccionaris i un string amb 
	el criteri d'ordenacio,	torna una llista amb la taula corresponent
	
	Entrada: diccionari clau:{valors}, str
	Sortida:
	'''
	table = []
	
	for key in diccio:
		table.append((key,diccio[key][val_ord]))
	
	return table
	

def dic_event(total, valor):
	'''
	Funció que ens retorna un subdiccionari del diccionari total, 
	que conté totes les claus que comparteixen valor. Si el valor 
	no existeix, retorna un diccionari buit
	
	
	Entrada: diccionari total {clau1:valor1,clau2:valor2...}
	Sortida: diccionari {clau1:valor1,clau3:valor1,clau5:valor1...}
	'''
	dic_sortida = {}
	
	for key in total:
		if total[key] == valor:
			dic_sortida[key] = valor
	
	return dic_sortida
	
def upload_dic(dicc, llista):
	'''
	Actualitza un diccionari 
	
	Entrada: dicc {clau:valor}, llista str
	Sortida:
	'''
	
	for item in llista:
		if not len(item) in dicc:
			dicc[len(item)] = 1
		else:
			dicc[len(item)] += 1
	
	return
		

def histograma(diccion,ll_ordre):
	'''
	Realitza un histograma del diccionari
	
	Entrada: diccionari , llista taula ordenada
	Sortida:
	'''
	for clau in ll_ordre:
		if not isinstance(clau,tuple):			# Serveix tan per llista elements (.keys) o per tuples
			print "%s: %s" % (clau,'*' * diccion[clau])
		else:
			print "%s: %s" % (clau[0],'*' * diccion[clau[0]])
	return
	
def inicia_dicc(referencia):
	'''
	Donada un string o llista de string, retorna un diccionari amb
	totes les keys iniciades a zero
	'''
	
	dicci = {}
	
	for elem in referencia:
		dicci[elem] = 0
		
	return dicci

def conta_freq(seq, referencia):
	'''
	Donada un string o llista de strings, retorna quantes vegades
	es repeteix cada caracter / cadena
	
	Entrada: string , string caracters
			 llista string , llista string
	Sortida: diccionari {elem:freq}
	'''
	
	solucio = inicia_dicc(referencia)
	
	for elem in seq:
		if elem in referencia:
			solucio[elem] += 1

				
	return solucio
	
def freq_in_str(llista):
	'''
	Donada una llista de paraules, retorna un diccionari amb la freqüencia
	de cada paraula
	
	Entrada: llista str
	Sortida: diccionari {paraula:freq}
	'''
	
	dicci = {}

	for clau in llista:
		if not dicci.has_key(clau):
			dicci[clau] = 1
		else:
			dicci[clau] += 1
	
	return dicci
	
def compara_camp(llista1,llista2):
	'''
	Donada dues llistes, retorna si X camp es es més gran, més petit o igual
	'''
	CAMP = 1
	if llista1[CAMP] > llista2[CAMP]:
		sol = 1
	elif llista1[CAMP] < llista2[CAMP]:
		sol = -1
	else:
		sol = 0
		
	return sol
	

		
		
