

#modul per a fer funcionar la nostra agenda  VERSIÓ 2

#utilitza diccionaris de dia del tipus
#  d_dia = {'21:00': {'nom': 'sopar', 'prioritat': 3, 'durada': '00:30'}}

#i diccionari agenda del tipus  (quan cal)
#       d_agenda = {'30/05/2018' : d_dia}


#Aquestes són només algunes de les funcions que podeu necessitar, però
#NO son obligatòries


def data_fitxer(data):
    '''
    Converteix una data en format 'dd/mm/aaaa' en un nom d'un fitxer del
    tipus 'dd-mm-aaaa.txt'
    Podeu utilitzar el mètode replace de strings
    Entrada: str que representa una data correcta
    Sortida str (que per a nosaltres serà un nom d'un fitxer)   
    '''
           

def mostra_event():
    '''
    Mostra les dades d'un event
    Entrada: 
    Sortida: res (escriu directament al fluxe)
    '''
   
    
def llistat_taula():
    '''
    Fa un llistat seguint l'ordre de la taula
    Entrada: diccionari d'events, taula
    Sortida: res (escriu directament al fluxe)
    '''
    


def llistat():
    '''
    Fa un llistat per defecte, és a dir, ordenat cronològicament.
    Ens evita haver d'escriure un sort al programa cada vegada. És
    una funció OPTATIVA (si no, es pot utilitzar sempre el llistat_taula
    Entrada: diccionari d'events, fluxe
    Sortida: res (escriu directament al fluxe)
    '''
    
def afegeix_dia():
    ''' 
    Funció que donat un diccionari_agenda que es passa per REFERÈNCIA,
    afegeix les activitats del dia que llegeix del fluxe de fitxer.
    (no us preocupeu si és 'curta').
    NOMÉS S'HA D'UTILITZAR QUAN HEU DE CARREGAR TOTES LES DADES
    Entrada:
    Sortida:
    '''
