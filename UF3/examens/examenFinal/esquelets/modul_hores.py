#!/usr/bin/python
#-*- coding: utf-8-*-

#modul que treballa amb hores de rellotge del tipus hh:mm  05:15
#Tant significa una hora en el rellotge (les 5 i quart) com la durada
#d'una activitat.

#No calen funcions per a controlar errors, només les imprescindibles
#per a definir la funció 'suma hores' pensant en mòduls de funcions
#(encapsulament, reusabilitat, flexibilitat).

#Exercici 0

    
def suma_hores(hora1, hora2):
    '''
    Funció que 'suma' dues hores de rellotge. Ens indica en quin moment 
    acabarà una activitat que comença en hora1 i dura tantes hores i 
    minuts com hora2 
    Entrada: hora de rellotge
    Sortida: hora de rellotge
    '''
    

    
if __name__ == "__main__":
    
    
    
