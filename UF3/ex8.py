#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#DICCIONARIS
#ex 8 diccionaris
#------------------------------------------
    # 8. Fes un programa que compti les repeticions de les paraules que surten en una frase .
       # Per exemple, per la següent cadena: 
       # cadena="hola hola tres cadena tres tres" 
       # Hem de generar el següent diccionari 
       # d={"hola":2,"tres":3,"cadena":1} 
       
def contar_repeticiones(secuencia):
	'''
	input: secuencia, es una llista
	ouput: diccionari {element:5}
	'''
	d={}
	for element in secuencia:
		if element in d:
			d[element]+=1
		else:
			d[element]=1
			
	return d

def contar_repeticiones_get(secuencia):
	'''
	input: secuencia
	ouput: diccionari {element:5}
	'''
	d={}
	for element in secuencia:
		if d.get(element) == None:
			d[element]=1
		else:
			d[element]+=1
			
	return d	

#entrada
text=input()

#hacemos lista con split para que separe por palabras, sino solo contaria char a char
l_text=text.split()

#comptar repeticions
d=contar_repeticiones(l_text)

print(d)
print("palabras diferentes:",len(d))
print("palabra total:",len(l_text))
#fer histograma
