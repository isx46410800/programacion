#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#ORDENA ALUMNES
##Fes que el programa ordena_alumnes.py funcioni, definint les funcions d'acord al seu docstring i fent que la sortida s'assembli a això:
#alumnes sense ordenar:
#jordi perez, dni: 12345678A, mitjana: 6
#manel garcia, dni: 52345678A, mitjana: 2
#...

#lista general de alumnos
dadesAlumnes = [['12345678A', 'jordi perez', [5.2, 8, 9, 4.3]], ['52345678A',
	'manel garcia', [2, 1.5, 3, 5]], ['62345678A', 'joan-ramon vila', [7,7,8,10]],
	 ['72345678A', 'maria casas', [7, 2.5, 9, 3]], ['111111111D', 'josep-lluís márquez',
	 [3, 10, 5]]]
	 
def lista_to_dicc(l_listas):
	'''
	input: lista de listas
	ouput: diccionario
	'''
	dic_total={}
	
	for pos in range(0, len(l_listas)):
		dic_valor={}
		#print(pos, l_listas[pos])
		dic_valor['Nombre Apellidos']=l_listas[pos][1]
		dic_valor['Notas']=l_listas[pos][2]
		dic_total[l_listas[pos][0]]=dic_valor
		
	return dic_total
	
def alumne_to_string(dic_total, dni):
	'''
	input: diccionario y dni como campo clave
	output: string
	'''
	return f"{dic_total[dni]['Nombre Apellidos']}, dni:{dni}, mitjana:{mitjana(dic_total[dni]['Notas'])}"

def mitjana(notas):
	'''
	input: diccionario
	output: int
	'''
	suma=0
	for nota in notas:
		suma = suma + nota
		mitjana = suma // len(notas)
	
	return int(mitjana)
	
def get_campo2(lista):
	'''
	input: lista de dos elementos
	output: lista del segundo campo
	'''
	return lista[1]
	
def get_campo1(lista):
	'''
	input: lista de dos elementos
	output: lista primer campo
	'''
	return lista[0]	
	
def dni_apellido_tabla(diccionario):
	'''
	input:dic
	output: lista de listas
	'''
	lista_apellido=[]
	for k in diccionario:
		lista=[]
		lista.append(k) #añade el campo key, el contenido del DNI
		lista.append(diccionario[k]['Nombre Apellidos'].split()[1])#añade el contenido del diccionario dentro del diccionario, coge del diccionario dni, el campo nombre apellidos, cogiendo solo el apellido
		lista_apellido.append(lista)
		#print(lista_apellido) [['12345678A', 'perez'], ['52345678A', 'garcia'], ['62345678A', 'vila'], ['72345678A', 'casas'], ['111111111D', 'márquez']]
	
	return lista_apellido
	
def dni_mitjana_tabla(diccionario):
	'''
	input:dic
	output: lista de listas
	'''	
	lista_dni=[]
	for k in diccionario:
		lista=[]
		lista.append(k) #añade el campo key, el contenido del DNI
		#print(lista)
		lista.append(mitjana(diccionario[k]['Notas'])) #añade el contenido del diccionario dentro del diccionario, coge del diccionario dni, el campo notas
		#print(lista)
		lista_dni.append(lista)
		#print(lista_dni) [['12345678A', 6], ['52345678A', 2], ['62345678A', 8], ['72345678A', 5], ['111111111D', 6]]

	return lista_dni

def llistat(taula, dic_total):
	'''
	input: taula concreta i dicc total
	ouput: void
	'''
	for d in taula:
		print(alumne_to_string(dic_total,d[0]))	
	
###PROGRAMA
diccionario=lista_to_dicc(dadesAlumnes)
print('---DE LISTA A DICCIONARIO---')
print(diccionario)

#ordena por apellido
print('---ORDENA POR APELLIDO---')
lista_apellido=[]
lista_apellido=dni_apellido_tabla(diccionario)
lista_apellido.sort(key=get_campo2)

llistat(lista_apellido,diccionario)
# for dni in lista_apellido:
	# print(alumne_to_string(diccionario,dni[0]))
print('--------------------------------------')

#ordena por media ascendente
print('---ORDENA POR MITJANA ASC---')
lista_mitjana=[]
lista_mitjana=dni_mitjana_tabla(diccionario)
lista_mitjana.sort(key=get_campo2)

llistat(lista_mitjana,diccionario)
# for dni in lista_mitjana:
	# print(alumne_to_string(diccionario,dni[0]))
print('--------------------------------------')

#ordena por media	descendente
print('---ORDENA POR MITJANA DES---')
lista_mitjana=[]
lista_mitjana=dni_mitjana_tabla(diccionario)
lista_mitjana.sort(key=get_campo2)
lista_mitjana.reverse()

llistat(lista_mitjana,diccionario)
# for dni in lista_mitjana:
	# print(alumne_to_string(diccionario,dni[0]))
print('--------------------------------------')
	
#ordena por dni	
print('---ORDENA POR DNI---')
lista_dni=[]
lista_dni=dni_mitjana_tabla(diccionario)
lista_dni.sort(key=get_campo1)

llistat(lista_dni,diccionario)
# for dni in lista_dni:
	# print(alumne_to_string(diccionario,dni[0]))
	
#-------test driver-------------------------
if __name__ == '__main__' :
    if False:
        print('-----LISTAS TO DICCIONARIO------')
        print(lista_to_dicc(dadesAlumnes))
        
        print('---ALUMNE TO STRING---')
        print(alumne_to_string(lista_to_dicc(dadesAlumnes),'12345678A'))
        print(alumne_to_string(lista_to_dicc(dadesAlumnes),'52345678A'))
        print(alumne_to_string(lista_to_dicc(dadesAlumnes),'62345678A'))
        print(alumne_to_string(lista_to_dicc(dadesAlumnes),'72345678A'))
        print(alumne_to_string(lista_to_dicc(dadesAlumnes),'111111111D'))
        
        print(mitjana([10,10,4])) #8
        
        print(get_campo2(['46410800C','Miguel Amoros'])) #'Miguel Amoros'
        print(get_campo1(['46410800C','Miguel Amoros'])) #46410800
