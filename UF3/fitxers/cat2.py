#!/usr/bin/python
#-*- coding: utf-8-*-
#
#Fòrmula de try / excepts definitiva

#AtenciÓ: NO és programació imperativa. Es fa el finally tot i haver lle
#git l'exit()

import sys

try:
	fluxe_fitxer = open('prova1.txt', 'r')
	try:
		#llegeixo i mostro
		for linia in fluxe_fitxer:
			sys.stdout.write(linia)
	except IOError:
		sys.stderr( 'error en la lectura')
		exit(1)	
	finally:
		fluxe_fitxer.close()
except IOError:
	sys.stderr.write('error')
	exit(2)
	
#Aquí es processaria tot el que no depèn del fitxer
