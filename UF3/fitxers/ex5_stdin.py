#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#EXERCICI 5 FITXERS
# 5. Fes un programa que simuli la ordre head.
       # La funcionalitat del programa haurà de ser la següent:
       # head   -num   nomFitx	: Mostrarà les num primeres línies del fitxer nomFitx
       # head   -num			: Mostrarà les num primeres línies de l'entrada 					stàndard
       # head   nomFitx		: Mostrarà les 10 primeres línies del fitxer nomFitx
       # head				: Mostrarà les 10 primeres línies de l'entrada 					stàndard
       # En cas que desprès del guió no hi hagi un nombre o que el fitxer no existeixi, mostrarà un missatge però no petarà.

STDIN='-'

import sys

LINIES=10

#CONTROL DE ARGUMENTS
nom_fitxer=STDIN

if len(sys.argv)==2:
	nom_fitxer=sys.argv[1]
elif len(sys.argv)>2:
	print("Error de args")
	exit(1)
	
#OBRIR EL FITXER
fluxe_fitxer=sys.stdin

if nom_fitxer!=STDIN
	flutxe_fitxer=open(nom_fitxer,'r')
	
#LECTURA
head(fluxe_fitxer,LINIES)

#TANQUEM
if nom_fitxer!=STDIN:
	fluxe_fitxer.close()
	
