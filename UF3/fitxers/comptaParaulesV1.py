# !/usr/bin/python
# -*- coding: utf-8-*-

# HISX1

#Exercicis de fitxers
#Exercici que compta les paraules d'un fitxer passat per argument
#v1: sense try except

import sys

nomFitxer = sys.argv[1]

# Obrir el fitxer o Entrada standard
flux_fitx = open(nomFitxer,'r')

# Lectura	
suma = 0	
for linea in flux_fitx :
	paraules = linea.split()
	suma += len(paraules)

# Tancar Fitxer
flux_fitx.close()

#processar les dades
print suma
