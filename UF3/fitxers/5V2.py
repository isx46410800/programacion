# !/usr/bin/python
# -*-coding: utf-8-*-
#nom autor diego sanchez piedra
#isx2031424

import sys

def head(f_file,n_lines):
    '''
    input=f de fitxer 'r', in
    ouput=void
    '''
    #inciem comptador
    cont = 0
    #iniciem la variable NO buida.
    linia='a'
    #per cada linia
    while (linia != '') and cont < n_lines:
        #llegim linia del fitxer
        linia=f_file.readline()
        #la mostrem per stdout
        sys.stdout.write(linia)
        #augmentem el comptador
        cont += 1

LINES = 10
#control arguments
STDIN = '-'
fileName = STDIN

if len(sys.argv) == 2:
    fileName = sys.argv[1]

elif len(sys.argv) > 2:
    print('error: too many arguments given')
    exit(1)

#obrir el fitxer
fluxe=sys.stdin
if fileName != STDIN:

    fluxe = open(sys.argv[1],'r')

#Lectura
head(fluxe,LINES)

#tancar el fitxer
if fileName != STDIN:
    fluxe.close()
