#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#EXERCICI 1 FITXERS
# 1. Fes un programa compti les línies d'un fitxer passat per argument.

import sys

#abrir fichero
fitxer = sys.argv[1]
fluxe_fitxer = open(fitxer,'r')

count=0

#leo y muestro
for linia in fluxe_fitxer:
	count=count+1
	#sys.stdout.write(linia)
	sys.stdout.write(str(count)+' '+linia)

#cerramos fichero	
fluxe_fitxer.close()

#mostramos resultado
print('Lineas totales:',count)


