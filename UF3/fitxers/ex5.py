#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#EXERCICI 5 FITXERS
# 5. Fes un programa que simuli la ordre head.
       # La funcionalitat del programa haurà de ser la següent:
       # head   -num   nomFitx	: Mostrarà les num primeres línies del fitxer nomFitx
       # head   -num			: Mostrarà les num primeres línies de l'entrada 					stàndard
       # head   nomFitx		: Mostrarà les 10 primeres línies del fitxer nomFitx
       # head				: Mostrarà les 10 primeres línies de l'entrada 					stàndard
       # En cas que desprès del guió no hi hagi un nombre o que el fitxer no existeixi, mostrarà un missatge però no petarà.


# def head(flutxe_fitxer, n_linies):
	# '''
	# e: fluxe de fitxer 'r', int
	# s: void
	# '''

# import sys
# LINIES=10

# fluxe_fitxer=(open(sys.argv[1],'r')

# head(fluxe_fitxer, LINIES

# fluxe_fitxer.close()


import sys
fitxer = sys.argv[1]
fluxe_fitxer = open(fitxer,'r')

#constante HEAD que muestra 10 linias
LINIES=10

#contamos
cont=0

#mientras que la linia no tenga contenido vacio y sea menor de linias pedidas
linia='-'
print(linia)

while (linia != '') and cont<LINIES: 
	if (cont<LINIES):
		linia=fluxe_fitxer.readline()
		sys.stdout.write(linia)
	cont+=1

#cerramos fichero	
fluxe_fitxer.close()
