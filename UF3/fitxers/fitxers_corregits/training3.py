import sys

sortida_stdar = True

if len(sys.argv) > 1:
	
	fix_final = sys.argv[1]
	
	sortida_stdar = False
	

nomFitxer = 'trainings/oficinas.dat'

fluxe = open (nomFitxer,'r')

objetivo = 0
ventas = 0
count = 0

for element in fluxe:
	
	element = element.strip()
	
	l = element.split('\t')
	
	objetivo = objetivo + int(l[4])
	ventas = ventas + int(l[5])
	count = count + 1
	
fluxe.close()

# print ((ventas / count), (objetivo / count))

resultat = ((ventas / count), (objetivo / count))

resultat = str(resultat)

# mostrar

fluxe_sortida = sys.stdout

if not sortida_stdar:

	fluxe_sortida = open (fix_final, 'w')

fluxe_sortida.write (resultat)

fluxe_sortida.close()
