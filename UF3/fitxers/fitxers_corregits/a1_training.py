# !/usr/bin/python3
# -*-coding: utf-8-*-

# JORDI QUIRÓS BERBEL
# isx48172244
# 23/05/2019
# versió 1.0


#ENUNCIAT: training

#ESPECIFICACIONS D'ENTRADA:

#-------------------------------------------------------------------

#REFINAMENT i CODI:

#IMPORTS
import sys

#FUNCIONS

#MAIN

#CONSTANTS

#CONTROL d'ERRORS

#PROGRAMA

#Comprobació arguments

if len(sys.argv) > 2:

    sys.stderr.write(f"ERROR: Number of args out of range.\n")
    exit(1)

to_file = False

if len(sys.argv) == 2:

    to_file = True
    file_name = sys.argv[1]

#Obrim el fitxer de training

flux_training = open('repventas.dat', 'r')

#Obrim el fitxer d'escritura o no
flux_exit = sys.stdout

if to_file:

    flux_exit = open(file_name, 'w')

#Per cada linia de training mirem si cumpleix la condicio
for line in flux_training:

    rep = line.strip().split('\t')

    ventas = rep[8]
    cuota = rep[7]

    if (ventas.isdigit() and cuota.isdigit()):

        ventas = int(ventas)
        cuota = int(cuota)

        #Si la cumpleix l'escrivim
        if not (ventas >= 0.8*cuota and ventas <= 1.2*cuota):

            flux_exit.write(f"{rep[1]},{ventas},{cuota}\n")

#Tanquem el/s fitxer/s
if to_file:

    flux_exit.close()

flux_training.close()
