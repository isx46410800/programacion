def organitzar (fluxe, guid):
	
	'''entrada: fluxe i int
	sortida: diccionari'''
	
	d = {}
	
	# per cada element del fluxe
	
	for linia in fluxe:
		
		linia = linia.strip()
		
		# la separem
		
		nom = []
		
		nom = (linia.split(':'))
		
		# creem un diccionari
		
		if nom[3] == guid:
			
			dicc = {}
			
			dicc['uid'] = nom[2]
			dicc['guid'] = guid
			dicc['gcos'] = nom[5]

			d[nom[0]] = dicc
		
	return d
	
# ----------------------------------------------------------------------

import sys

nomFitxer = '/etc/passwd'

guid = sys.argv[1]

# Obrim el fitxer treballem amb ell y el tanquem

fluxe = open (nomFitxer,'r')

diccionari = organitzar(fluxe,guid)

fluxe.close()

# pasem diccionari a llista y la ordenem

llista = list (diccionari.items())

llista.sort()

# printem

for linia in llista:
	
	print (linia[0], linia[1]['uid'], linia[1]['guid'], linia[1]['gcos'])

