
import sys

#el nombre de fichero por argumento
nomFitxer = sys.argv[1]

#abrimos el fichero
fluxe = open (nomFitxer,'r')

#lista vacia de nombres
nom = []

#para cada linia del fichero, 
for linia in fluxe:
	#separamos la linia por : y añadimos a la lista el campo 5
	nom.append(linia.split(':')[4])
	
#cerramos el fichero
fluxe.close()

#ordenamos la lista
nom.sort()

#printamos cada linia ordenada
for linia in nom:
	
	if linia != '':
	
		print(linia)
