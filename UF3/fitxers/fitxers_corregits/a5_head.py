# !/usr/bin/python3
# -*-coding: utf-8-*-

# JORDI QUIRÓS BERBEL
# isx48172244
# 15/05/2019
# versió 1.0


#ENUNCIAT: 5. Fes un programa que simuli la ordre head.

#ESPECIFICACIONS D'ENTRADA: head.py
#                           head.py -10
#                           head.py file.txt
#                           head.py -10 file.txt


#-------------------------------------------------------------------

#REFINAMENT i CODI:

#IMPORTS

import sys

#CONSTANTS

STDIN = '-'
LINES = 10

#FUNCIONS

def head(f,l):

    '''
    Funció que donats un flux de dades d'un fitxer "f" i un número de línies,
    printa només les "l" primeres linies del fitxer.
    INPUT = fluxe de fitxer de lectura i un int > 0.
    RETURN = Void.
    '''

    i = 0

    while (f.readline() != "") and (i < l):

        sys.stdout.write(f.readline())

        i += 1

    return


#MAIN

#CONTROL d'ERRORS

# Control d'arguments

if len(sys.argv) > 3:

    sys.stderr.write(f"ERROR: Number of args is out of range. \n")
    exit(1)

file_name = STDIN

if len(sys.argv) == 3:

    arg1 = sys.argv[1]
    num = arg1[1:]
    arg2 = sys.argv[2]

    if '-' not in arg1:

        sys.stderr.write(f"ERROR: Input arg is not an option.\n")
        exit(2)

    elif not num.isdigit():

        sys.stderr.write(f"ERROR: Input arg is not a number.\n")
        exit(3)

    LINES = int(num)

    file_name = arg2

elif len(sys.argv) == 2:

    arg1 = sys.argv[1]

    if '-' in arg1:

        num = arg1[1:]

        if not num.isdigit():

            sys.stderr.write(f"ERROR: Input arg is not a number.\n")
            exit(3)

        else:

            LINES = int(num)

    else:

        file_name = arg1

# Obrim

flux = sys.stdin

if file_name != STDIN:

    flux = open(file_name, 'r')

# Llegim

head(flux, LINES)

# Tanquem

if file_name != STDIN:

    flux.close()
