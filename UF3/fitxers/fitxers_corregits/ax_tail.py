# !/usr/bin/python3
# -*-coding: utf-8-*-

# JORDI QUIRÓS BERBEL
# isx48172244
# data
# versió 1.0


#ENUNCIAT:

#ESPECIFICACIONS D'ENTRADA:


#-------------------------------------------------------------------

#REFINAMENT i CODI:

#IMPORTS

import sys

#CONSTANTS


#FUNCIONS

def tail(f,l):

    '''
    Funció que donats un flux de dades d'un fitxer "f" i un número de línies,
    retorna una llista amb només les "l" últimes linies del fitxer.
    INPUT = fluxe de fitxer de lectura i un int > 0.
    RETURN = List of strings.
    '''

    llista = []

    for line in f:

        if len(llista) != LINES:

            llista += [line.strip()]

        else:

            llista = llista[1:]
            llista += [line.strip()]

    return llista

#MAIN

#CONTROL d'ERRORS

#PROGRAMA

file_name = sys.argv[1]

LINES = 10

flux = open(file_name, 'r')

l = tail(flux,LINES)

flux.close()

for line in l:

    print(line)
