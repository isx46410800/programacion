# !/usr/bin/python
# -*- coding: utf-8-*-

# HISX1

#Exercicis de fitxers
#Exercici que compta les paraules d'un fitxer passat per argument
#si no hi ha fitxer, es processa stdin
#v4: amb try except

import sys
STDIN = '-'

#control d'arguments (fixeu-vos que està fora dels try)
if len(sys.argv) == 1:
	#agafar stdin
	nomFitxer = STDIN

else:
	#agafar el fitxer
	nomFitxer = sys.argv[1]



# Obrir el fitxer o Entrada standard
try:
	flux_fitx = sys.stdin
	
	if nomFitxer != STDIN:
		flux_fitx = open(nomFitxer,'r')
	
	# Lectura
	try:	
		suma = 0	
		for linea in flux_fitx :
			paraules = linea.split()
			suma += len(paraules)
	except IOError:
		sys.stderr( 'error al processar el fitxer %s'%nomFitxer)
		exit()
	
	finally:		
		# Tancar Fitxer
		if nomFitxer != STDIN :
			flux_fitx.close()
	
except IOError:
	sys.stderr( 'error en obertura del fitxer %s' %nomFitxer)
	exit()
	
#processar les dades
print suma
