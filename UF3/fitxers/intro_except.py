#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

try:
	fluxe_fitxer = open('prova.txt','r')
	
	try:
		#llegeixo i mostro
		for linia in fluxe_fitxer:
			sys.stdout.write(linia)
	except:
		print('Error en la lectura')
		exit(1)
	finally:
		fluxe_fitxer.close()
except:
	print('Error')
	exit(2)

#aqui es processaria tot el que no depen del fitxer
