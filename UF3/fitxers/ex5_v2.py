#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#EXERCICI 5 FITXERS
# 5. Fes un programa que simuli la ordre head.
       # La funcionalitat del programa haurà de ser la següent:
       # head   -num   nomFitx	: Mostrarà les num primeres línies del fitxer nomFitx
       # head   -num			: Mostrarà les num primeres línies de l'entrada 					stàndard
       # head   nomFitx		: Mostrarà les 10 primeres línies del fitxer nomFitx
       # head				: Mostrarà les 10 primeres línies de l'entrada 					stàndard
       # En cas que desprès del guió no hi hagi un nombre o que el fitxer no existeixi, mostrarà un missatge però no petarà.


import sys
fitxer = sys.argv[1]
fluxe_fitxer = open(fitxer,'r')

#constante HEAD que muestra 10 linias
LINIES=10

#contamos
cont=0

#leo y muestro v1 pero leeria todas las linias del fichero
for linia in fluxe_fitxer:
	if (cont<LINIES):
		sys.stdout.write(linia)
	cont+=1

#cerramos fichero	
fluxe_fitxer.close()
