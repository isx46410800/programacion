# !/usr/bin/python
# -*- coding: utf-8-*-

# HISX1

#Exercicis de fitxers
#Exercici que compta les paraules d'un fitxer passat per argument
#si no hi ha fitxer, es processa stdin
#v3a: sense try except

import sys
STDIN = '-'

#control d'arguments
if len(sys.argv) == 1:
	#agafar stdin
	nomFitxer = STDIN

else:
	#agafar el fitxer
	nomFitxer = sys.argv[1]



# Obrir el fitxer o Entrada standard
flux_fitx = sys.stdin

if nomFitxer != STDIN:
	flux_fitx = open(nomFitxer,'r')

# Lectura	
suma = 0	
for linea in flux_fitx :
	paraules = linea.split()
	suma += len(paraules)

# Tancar Fitxer
flux_fitx.close()

#processar les dades
print( suma)
