# !/usr/bin/python
# -*- coding: utf-8-*-

# HISX1

#Exercicis de fitxers
#Exercici que compta les paraules d'un fitxer passat per argument
#v2: amb try except

import sys

#falta el control d'errors
nomFitxer = sys.argv[1]

try:# aquest try és per si no existeix el fitxer
	# Obrir el fitxer o Entrada standard
	flux_fitx = open(nomFitxer,'r')
	
	suma = 0
	# Lectura	
	try	:
		for linea in flux_fitx :
			paraules = linea.split()
			suma += len(paraules)
	
	# Tancar Fitxer
	except IOError:
		sys.stderr( 'error al processar el fitxer %s'%nomFitxer)
		exit()
		
	finally:
		# Tancar Fitxer  (tant si ha petat com si no)
		flux_fitx.close()
			
except IOError:
	sys.stderr( 'error en obertura del fitxer %s' %nomFitxer)
	exit(1)  #què passa si no hi és ?
	
#processar les dades
print( suma)
