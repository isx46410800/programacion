#!/usr/bin/python
#-*- coding: utf-8-*-
#
#Per veure un primer try except, que controla si no existeix el fitxer

import sys

try:
	fluxe_fitxer = open('prova1.txt', 'r')
	
	#llegeixo i mostro
	for linia in fluxe_fitxer:
		sys.stdout.write(linia)
	
	fluxe_fitxer.close()

except:
	print 'error'
	
