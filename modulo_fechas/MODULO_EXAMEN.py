import datetime

def correct_format(a):

    '''
    Given a str that represents a date,
    it verifies that is a correct date (DD/MM/YYYY)
    INPUT = str that represents a date
    OUTPUT = Bool
    '''

    if not (len(a) == 10):

        return False

    num = a[0:2] + a[3:5] + a[6:]
    are_slash = a[2] == "/" and a[5] == "/"

    if not (num.isdigit() and are_slash):

        return False

    return True

def leap_year(a):

    '''
    Given a int that represents a year, says if its a leap year or not
    INPUT= int > 0 (year)
    OUTPUT = BOOL
    '''

    return (a % 400 == 0 or (a % 4 == 0 and not a % 100 == 0))



def get_day(a):

    '''
    Given a str that represents a correct froamt date (DD/MM/YYYY),
    extracts de day of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    day = int(a[0:2])

    return day

def get_month(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de month of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    month = int(a[3:5])

    return month

def get_year(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de year of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    year = int(a[6:10])

    return year


def last_day_of_month(a):

    '''
    Given a str that represents a correct date, says the last day of that month.
    INPUT = str (correct date)
    OUTPUT= int
    '''

    month = get_month(a)
    year = get_year(a)

    last_days = [0,31,28,31,30,31,30,31,31,30,31,30,31]

    # IF ITS FEBRUARY AND A LEAP-YEAR

    if month == 2 and leap_year(year):

        last_days[2] = 29

    return last_days[month]


def valid_date(a):

    '''
    Given a str that represents date, says if the input string is a correct date.
    INPUT = str in the form of XX/XX/XXXX where X is an int
    OUTPUT = Bool
    '''

    if not correct_format(a):

        return False

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)


    if year < 0:

        return False

    if not (month >= 1 and month <= 12):

        return False

    if not (day >= 1 and day <= last_day_of_month(a)):

        return False

    return True

def set_date(d,m,y):

    '''
    Given 3 ints > 0 that represent a day, month and year respectively,
    creates a str in the form of DD/MM/YYYY
    INPUT = 3 ints > 0
    OUTPUT = 1 str (DD/MM/YYYY)
    '''

    return f"{d:02}/{m:02}/{y:04}"


def today():

    '''
    Function that returns the current date like DD/MM/YYYY
    INPUT = None
    OUTPUT = str (correct date DD/MM/YYYY)
    '''
    t = datetime.datetime.now()
    return set_date(t.day,t.month,t.year)

def next_day(a):

    '''
    Given a str that represents a corrent date,
    it returns the following day.
    INPUT = str (correct date DD/MM/YYYY)
    OUTPUT = str (correct date DD/MM/YYYY)
    '''

    # Extract day, month and year

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)

    # Increase the day by 1

    day += 1

    # Check if the day is a correct date and make the necessary changes

    # If the day is not corect

    if day > last_day_of_month(a):

        day = 1
        month += 1

    # If the month is not correct

    if month > 12:

        month = 1
        year += 1

    return set_date(day,month,year)

def has_my_bd_passed(date, birthday):

    '''
    Function that given 2 correct dates (in the form of DD/MM/YYYY), being the
    first one any date and the second one being the birthday,
    returns an int (-1 or 0 or 1) depending on whether it has
    been (-1), it is (0) or it will be (1).
    '''

    status=1

    bd_day= get_day(birthday)
    bd_month= get_month(birthday)

    d_day= get_day(date)
    d_month= get_month(date)

    if bd_month < d_month:

        status = -1

    elif bd_month == d_month:

        if bd_day < d_day:

            status = -1

        elif bd_day == d_day:    #HAPPY BIRTHDAY :D

            status = 0

    return status


def has_my_bd_passed(dia_,mes_a):
	'''
	'''
	#trobo la data d'avui
	
	#creo la data aniversari aniv=dia_a/mes_a/any_actual
	
	#si la data ha passat
	
		aniv= dia_a/mes_a/(any_actual+1)
		
	#vaig passant dies fins a arribar a l'aniversari
	
	#return dies
	
# def compara_dates(d1,d2):
	# '''
	# '''
	
	# return int(1 si d1>d2, 0 si son igual, -1 si d1<d2)


if __name__ == '__main__':

    #TEST DRIVES

    if False:

        print('correct format')
        print(correct_format('3')) #False
        print(correct_format('08/061/997')) #False
        print(correct_format('08/06/1997')) #True

    if False:

        print('leap_year')
        print(leap_year(2008)) #True
        print(leap_year(2007)) #False

    if False:

        print('get_day')
        print(get_day('08/06/1997')) # 8
        print(get_day('99/06/1997')) # 99

    if False:

        print('last_day_of_month')
        print(last_day_of_month('08/06/1997')) #30
        print(last_day_of_month('08/02/1997')) #28
        print(last_day_of_month('08/01/1997')) #31
        print(last_day_of_month('08/02/2000')) #29

    if False:

        print('valid_date')
        print(valid_date('08/06/1997')) #True
        print(valid_date('08/06/20188')) #False
        print(valid_date('33/06/1997')) #False
        print(valid_date('08/14/1997')) #False
        print(valid_date('29/02/2000')) #True

    if False:

        print('set_date')
        print(set_date(8,6,1997))
        print(set_date(1,1,1))
        print(set_date(99,99,9999))

    if False:

        print('next_day')
        print(next_day('01/01/2000')) # 02/...
        print(next_day('28/02/2000')) # 29/...
        print(next_day('29/02/2000')) # 01/03...
        print(next_day('31/01/2000')) # 01/02/...
        print(next_day('31/12/2000')) # 01/01/2001

    if False:

        print('has_my_bd_passed')
        print(has_my_bd_passed('01/01/2019','08/06/1997')) #1
        print(has_my_bd_passed('31/12/2019','08/06/1997')) #-1
        print(has_my_bd_passed('08/06/1997','08/06/1997')) #0
