

#CAPÇALERA COMPLERTA O SERÂ UN 0
#Programa que ens calcula els n primers primers de Sophie Germain

import sys 
import funcions_primers 

#llegir el nombre de primers que volem
quant_primers = int(sys.argv[1])
s_primer = 2	#el primer primer és el dos

#bucle (hem de repetir quant_primers vegades)
for i in range(0, quant_primers):
	#el printem
	print( 's. germain primer: %d, primer segur: %d' % (s_primer, 2 * s_primer + 1))
	#busquem el següent primer de Sophie Germain
	s_primer = funcions_primers.seguent_sophie_primer(s_primer)
	
	

