#!/usr/bin/python
#-*-coding: utf-8*-
#################################################################
#NOM AUTOR:		Daniel Cano 
#ISX: 			isx53320079
#DATA: 			17/1/2018
#VERSIÓ: 		1.0
#################################################################
def valida_format_data(data):
	'''
	Funcio que valida el format d'una data
	Entrada: str en format data
	Sortida: bool
	'''
	if len(data) != 10:
		return False
	return data[:2].isdigit() and data[2] == '/' \
	and data[3:5].isdigit() and data[5] == '/' \
	and data[6:].isdigit()

def get_dia(data):
	'''
	Funcio que retorn el dia d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[:2])
	
def get_mes(data):
	'''
	Funcio que retorn el mes d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[3:5])

def get_any(data):
	'''
	Funcio que retorn el any d'una data
	Entrada: str en format data
	Sortida: int
	'''
	return int(data[6:])

def es_any_de_traspas(anyo):
	'''
	Funcio que diu si es any de traspas
	Entrada: int
	Sortida: bool
	'''
	return (anyo % 4 == 0 and anyo % 100 != 0) or (anyo % 400 == 0) 

def ultim_dia(data):
	'''
	Funcio que retorna l'ultim dia del mes
	Entrada: str en format data
	Sortida: int
	'''
	llista_dies = [0,31,28,31,30,31,30,31,31,30,31,30,31]
	mes = get_mes(data)
	if es_any_de_traspas(get_any(data)):
		llista_dies[2] = 29
	return llista_dies[mes]
	
def data_valida(data):
	'''
	Funcio que valida una data
	Entrada: str en format data
	Sortida: bool
	'''
	#es format correcte
	if not valida_format_data(data):
		return False
	#si la'any es incorrecte
	if get_any(data) < 0:
		return False
	mes = get_mes(data)
	dia = get_dia(data)
	return (mes >= 1 and mes <= 12) and \
	(dia <= ultim_dia(data) and dia >= 1)
		

def data_a_format_correcte(dia,mes,anyo):
	'''
	funcio que segons un dia, mes y any el pasa a format data
	entrada: 3 int
	retorn:string en format data
	'''
	return "%02d/%02d/%04d" % (dia,mes,anyo)

def dia_seguent(data):
	'''
	Funcio que retorna el dia seguent
	Entrada: str en format data
	Sortida: str en format data
	'''
	dia = get_dia(data)
	mes = get_mes(data)
	anyo = get_any(data)
	
	dia = dia + 1
	if dia > ultim_dia(data):
		dia = 1 
		mes = mes + 1
		if mes > 12:
			mes = 1
			anyo = anyo + 1
	return data_a_format_correcte(dia,mes,anyo)
	
# #Test driver
# if __name__ == "__main__":

# 	#valida data
# 	if False:
# 		print valida_format_data('12/12/2018')
# 		print valida_format_data('aa/aa/aaaa')
# 		print valida_format_data('12/03/0232')
	
# 	#get dia
# 	if False:
# 		print get_dia('12/12/2018')
# 	#get mes
# 	if False:
# 		print get_mes('12/10/2018')
# 	#get any
# 	if False:
# 		print get_any('12/12/2018')
# 	#es any de traspas
# 	if False:
# 		print es_any_de_traspas(2017)
# 		print es_any_de_traspas(2016)
# 	#ultim dia	
# 	if False:
# 		print ultim_dia('29/02/2017')
# 		print ultim_dia('29/02/2016')
# 		print ultim_dia('31/12/2016')
# 		print ultim_dia('32/11/2016')
# 	#data valida
# 	if True:
# 		print data_valida('29/02/2017')
# 		print data_valida('29/02/2016')
# 		print data_valida('31/12/2016')
# 		print data_valida('31/11/2016')
# 	#data a format correcte
# 	if False:
# 		print data_a_format_correcte(31,13,2016)
# 		print data_a_format_correcte(31,1,2016)
# 	#dia seguent
# 	if True:
# 		print dia_seguent('28/02/2016')
# 		print dia_seguent('28/02/2015')
# 		print dia_seguent('31/12/2016')
