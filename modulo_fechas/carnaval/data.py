#nom autor:     jorge pastor
#isx:           isx47787241
#data:          06/02/2019
#versió:        0.1

import sys
import time

# a) Crea una funció que validi el format de una data (dd/mm/yyyy)
def formato_correcto(data):
    """
    F. valida una data en format dd/mm/yyyy
    data_correcta( str )
    return bolean
    """
    dia = data[:2]
    mes = data[3:5]
    anyo = data[6:]
    correcta = True

    if len(data) != 10:
        correcta = False
    elif data[2:6:3] != '//':
        correcta = False
    elif not ( dia + mes + anyo ).isdigit() :
        correcta = False
    
    return correcta


# b) crea 3 funciones que extraigan dia, mes y año del formato  (dd/mm/yyyy) 
def day(fecha):
    """
    F. extrae dia del format (dd/mm/yyyy) 
    day( str )
    return int
    """
    fecha = fecha[:2]
    if fecha.isdigit():
        fecha = int(fecha)
    else:
        fecha = -1
    return  fecha

def month(fecha):
    """
    F. extrae mes del format (dd/mm/yyyy) 
    month( str )
    return int
    """
    fecha = fecha[3:5]
    if fecha.isdigit():
        fecha = int(fecha)
    else:
        fecha = -1
    return  fecha

def year(fecha):
    """
    F. extrae año del format (dd/mm/yyyy) 
    year( str )
    return int
    """
    fecha = fecha[6:]
    if fecha.isdigit():
        fecha = int(fecha)
    else:
        fecha = -1
    return  fecha


# c) crea una funcio que validi si un any es visest o no
def bisiesto(anio):
    """
    F. valida un año si es bisiesto o no
    bisiesto( int )
    return bolean
    """
    return anio % 400 == 0 or ( anio % 4 == 0 and anio % 100 != 0  )


# d) verifica meses y dias en año o no bisiestos
def mes_dia_correcto(anio, mes, dia):
    """
    F. valida numero de dias y meses correctos en una fecha
    mes_dia_correcto( int(año), int(mes), int(dia) )
    return bolean
    """
    valida = False
    if dia <= dias_mes(mes, anio) and ( mes > 0 and mes <= 12 ):
        valida = True
    
    return valida

# Crea una fincio retorna dias de un mes
def dias_mes(mes, anio):
    """
    F. funcion retorna dias que tiene un mes
    dias_mes( int(mes), int(año) )
    return int
    """
    if mes in [1,3,5,7,8,10,12]:
        dias = 31
    elif mes in [4,6,9,10,11]:
        dias = 30
    elif mes == 2 and bisiesto(anio):
        dias = 29
    else:
        dias = 28

    return dias



# *) Crea una funcio que validi una data amb el format (dd/mm/yyyy) 
def data_correcta_reton(data):
    """
    F. valida una data en format dd/mm/yyyy
    data_correcta( str )
    return bolean
    """
    
    return formato_correcto(data) and mes_dia_correcto(year(data), month(data), day(data) )

def mostrar_fecha(dia, mes, anio):
    """
    F. dados dia, mes y año lo muestra en formato dd/mm/yyyy
    mostrar_fecha( int(dia), int(mes), int(año) )
    return str
    """
    return f"{dia:02}/{mes:02}/{anio:04}"


def dia_siguiente(dia, mes, anio):
    """
    F. dados dia, mes y año calcula el dia siguiente
    dia_siguiente( int(dia), int(mes), int(año) )
    return str format(dd/mm/yyyy) 
    """
    # sumar un dia
    dia += 1

    # si el dia es superior al dias posibles de mes: dia a 1 y sumar un mes
    if dia > dias_mes(mes, anio) :
        dia = 1
        mes += 1

    # si el mes es superior a 12 sumar 1 año y mes a 1
    if mes > 12:
        anio += 1
        mes = 1

    return mostrar_fecha(dia, mes, anio)


def dia_anterior(dia, mes, anio):
    """
    F. dados dia, mes y año calcula el dia anterior
    dia_anterior( int(dia), int(mes), int(año) )
    return str format(dd/mm/yyyy) 
    """
    # sumar un dia
    dia -= 1

    # si el dia es inferior a 1 retrocedo un mes
    if dia < 1: 
        mes -= 1
        # si el mes es inferior a 1 retrocedo un año, mes es 12 y dias totales de dicho mes
        if mes < 1:
            anio -= 1
            mes = 12
            dia = dias_mes(mes, anio)
        # sino dias es el total de dias del mes anterior
        else:
            dia = dias_mes(mes, anio)
            

    return mostrar_fecha(dia, mes, anio)


def dias_hasta_fin_mes(dia, mes, anio):
    """
    F. devuelve dias que faltan hasta el final de ese mes
    def dias_hasta_fin_mes( int(dia), int(mes), int(anio) )
    return int
    """
    dias_fin = dias_mes(mes, anio) - dia 
    return dias_fin

    

def dias_hasta_fin_anio(dia, mes, anio):
    """
    F. calcula lis dias desde unas fecha hasta fin de año
    """
    dias_fin_anio = dias_hasta_fin_mes(dia, mes, anio)
    mes += 1
    while mes <= 12 :   
        dias_fin_anio += dias_mes(mes, anio)
        mes += 1

    return dias_fin_anio

def dias_desde_principio_anio(dia, mes, anio):
    """
    F. cuenta dias desde principio de año hasta la fecha indicada en ese año
    dias_desde_principio_anio( int(dia), int(mes), int(anio) )
    return int
    """
    dias_anio = 0
    cont = 1
    while cont < mes :   
        dias_anio += dias_mes(cont, anio)
        cont += 1
    
    return dias_anio + dia

def dias_mes_ini_mes_fin(mesini, mesfin, anio):
    """
    F. cuenta dias desde mes inicio hasta mes final no incluido
    dias_mes_ini_mes_fin(mesini, mesfin, anio)
1    return dias
    """
    dias = 0 
    while mesini < mesfin :   
        dias += dias_mes(mesini, anio)
        mesini += 1
    
    return dias


def dias_para_aniversario(dia_nacimiento, mes_nacimiento):
    """
    F. dados un mes y dia  calcula dias hasta el cumpleaños de la fecha actual
    dias_para_aniversario( int(dia_nacimiento) , int(mes_nacimiento) )
    return int
    """
    
    # Extraigo fecha actual
    fecha_actual = time.strftime("%d/%m/%Y")
    dia_actual = day(fecha_actual)
    mes_actual = month(fecha_actual)
    anio_actual = year(fecha_actual)

    # mes actual es inferior al de nacimiento
    if mes_nacimiento > mes_actual:
     
    #     miro los dias que faltan hasta el mes de cumplir años
        dias = dias_mes_ini_mes_fin(mes_actual, mes_nacimiento, anio_actual)
        dias -= dia_actual
    #     sumo los dias del mes en que cumplo años
        dias += dia_nacimiento    

    # si el mes actual es superior al de nacimiento o
    #     el mes actual es igual al de nacimiento y el dia actual es superior al de nacimiento
    elif mes_actual > mes_nacimiento or ( mes_actual == mes_nacimiento and dia_actual > dia_nacimiento) :
    #     miro los dias hasta fin de año
        dias = dias_hasta_fin_anio(dia_actual, mes_actual, anio_actual)
        
    #     miro los meses del año siguiente hasta mi fecha de cumpleaños
        dias += dias_desde_principio_anio(dia_nacimiento, mes_nacimiento, anio_actual + 1 )

    # si el mismo mes y no a pasado el dia de aniversario
    #     resto dias de aniversario a los actuales
    else:
        dias = dia_nacimiento - dia_actual

    return dias



# Aniversario muy pco eficiente -------------------------------------------------------------
def dias_para_cumpleaños(fecha):
    """
    F. retorna los dias que faltan para tu aniversario.
    dias_para_cumpleaños( str(fecha de nacimiento) ) formato dd/mm/yyyy
    return int
    """
    # Extraigo fecha actual
    fecha_actual = time.strftime("%d/%m/%Y")

    # desgloso fecha actual y la introducida
    dia_nacimiento = day(fecha)
    mes_nacimiento = month(fecha)
    anio_nacimiento = year(fecha)

    dia_actual = day(fecha_actual)
    mes_actual = month(fecha_actual)
    anio_actual = year(fecha_actual)

    # mirar si ya a pasado mi cumpleaños
    if  mes_actual < mes_nacimiento or ( mes_actual == mes_nacimiento and dia_actual <= dia_nacimiento ) :
        # añadir año actual 
        anio_nacimiento = anio_actual
    else:    
        # o año siguiente al actual a mi fecha de nacimiento
        anio_nacimiento = anio_actual + 1

    # voy sumando dias hasta que coincida con mi fecha
    dias_para_aniversario = 0
    while mostrar_fecha(dia_actual,mes_actual, anio_actual) != mostrar_fecha(dia_nacimiento, mes_nacimiento, anio_nacimiento):
        
        nueva_fecha = dia_siguiente(dia_actual, mes_actual, anio_actual)

        dia_actual = day(nueva_fecha)
        mes_actual = month(nueva_fecha)
        anio_actual = year(nueva_fecha)
        
        dias_para_aniversario += 1

    return dias_para_aniversario
# ----------------------------------------------------------------------------------------------

def dia_de_semana(dia, mes, anio):
    """
    F. dados un dia, mes y año retorna a que dia de la semana pertenece en numerico, 
    donde 0 = domingo 6 = sabado 
    dia_de_semana( int(dia), int(mes), int(anio) )
    return int
    """

    a = ( 14 - mes ) // 12
    b = anio - a
    c = mes - 2  + a * 12
    d = b // 4  
    e = b // 100
    f = b // 400
    g = c * 31 // 12
    h = dia + b + d - e + f + g
    i = h % 7
    dia_semana = i

    return dia_semana

# Test drivers
if __name__ == "__main__":

    if False:
        print('dia de semana')
        print(dia_de_semana(7,3,2019))      # 4
        print(dia_de_semana(9,3,2019))      # 6
        print(dia_de_semana(10,3,2019))      # 0

    if False:
        print('dia_anterior')
        print(dia_anterior(1, 1, 2019))     #31/12/2018
        print(dia_anterior(1, 3, 1988))     #29/02/1988
        print(dia_anterior(5, 1, 2019))     #04/01/2019
        print(dia_anterior(31, 12, 2000))     #30/12/2000

    if False:
        print('Formato correcto ')
        print(formato_correcto('00/11/1234'))     # True
        print(formato_correcto('999999999999999')) # False
        print(formato_correcto('wiiiii'))         # false
        print(formato_correcto('08/10/2000'))     # True
        print(formato_correcto('patata'))         # False

    if False:
        print('dias')
        print(day('01/25/0000'))    # 1
        print(day('50/25/0000'))    # 50
        print(day('9999999999'))    # 99
        print(day('10'))             # 10
        print(day('yupi'))             # -1

    if False:
        print('meses')
        print(month('01/25/0000'))    # 25
        print(month('hola'))              # -1 
        print(month('015995000012'))  # 99
        print(month('01/00/0000'))    # 0

    if False:
        print('años')
        print(year('01/25/0000'))    # 0
        print(year('01'))             # -1    
        print(year('patataphola'))    # -1
        print(year('------2000'))    # 2000

    if False:
        print(bisiesto(1988))           # True
        print(bisiesto(1))              # False
        print(bisiesto(88))             # True
        print(bisiesto(1401988))       # True

    if False:
        print('dias mes')
        print(dias_mes(1,1988))         # 31
        print(dias_mes(12,2000))         # 31
        print(dias_mes(11,0))         # 30
        print(dias_mes(4,100))         # 30
        print(dias_mes(2,1988))         # 29
        print(dias_mes(2,30001))         # 28

    if False:    
        print('mes_dia_correcto')
        print(mes_dia_correcto(1988,2,29))      # True
        print(mes_dia_correcto(1989,2,29))      # False
        print(mes_dia_correcto(0,5,1))      # True
        print(mes_dia_correcto(100,12,31))      # True
        print(mes_dia_correcto(20,2,29))      # True

    if False:
        print('data_correcta_reton')
        print(data_correcta_reton('10/10/1988'))    # True
        print(data_correcta_reton('5/10/2000'))     # false
        print(data_correcta_reton('10/10/198800'))     # false
        print(data_correcta_reton('10-10-1988'))     # false
        print(data_correcta_reton('Hola mundo'))     # false
        print(data_correcta_reton('          '))     # false

    if False:
        print('mostrar_fecha')
        print(mostrar_fecha(1,20,1))            # 01/200001
        print(mostrar_fecha(-1,+20,1145))       # -1/20/1145
        print(mostrar_fecha(1145,2020,12014))   # 1145/2020/12014
        print(mostrar_fecha(0,20,11))           # 00/20/0011

    if False:
        print('dia_siguiente')
        print(dia_siguiente(31,12,2000))    # 01/01/2001
        print(dia_siguiente(29,2,1988))    # 01/03/1988
        print(dia_siguiente(28,2,1989))    # 01/03/1989
        print(dia_siguiente(28,2,1988))    # 29/02/1988
        print(dia_siguiente(30,11,20000))    # 01/12/20000

    if False:
        print('dias_hasta_fin_mes')
        print(dias_hasta_fin_mes(5,3,1050))     # 26
        print(dias_hasta_fin_mes(20,6,10))     # 10
        print(dias_hasta_fin_mes(28,2,100))     # 0
        print(dias_hasta_fin_mes(29,2,1988))     # 0
        print(dias_hasta_fin_mes(1,1,2019))     # 30

    if False:
        print('dias_hasta_fin_anio')
        print(dias_hasta_fin_anio(1,1,1))           # 364
        print(dias_hasta_fin_anio(31,12,2000))      # 0
        print(dias_hasta_fin_anio(20,2,10))         # 314
        print(dias_hasta_fin_anio(11,11,1111))      # 50
        print(dias_hasta_fin_anio(1,10,1050))       # 91

    if False:
        print('dias_desde_principio_anio')
        print(dias_desde_principio_anio(1,1,1))           # 1
        print(dias_desde_principio_anio(31,12,2000))      # 366
        print(dias_desde_principio_anio(20,2,10))         # 51
        print(dias_desde_principio_anio(11,11,1111))      # 315
        print(dias_desde_principio_anio(1,10,1050))       # 274
        
    if False:
        print('dias_mes_ini_mes_fin')
        print(dias_mes_ini_mes_fin(1,6,2000))       # 152
        print(dias_mes_ini_mes_fin(10,12,1999))       # 61
        print(dias_mes_ini_mes_fin(1,1,2000))       # 0
        print(dias_mes_ini_mes_fin(3,2,2000))       # 0
        print(dias_mes_ini_mes_fin(4,6,2000))       # 61
        
    if False:
        print('dias_para_aniversario en dia actual 06/03/2019')
        print(dias_para_aniversario(8,10))      # 216
        print(dias_para_aniversario(25,10))      # 233
        print(dias_para_aniversario(2,5))      # 57
        print(dias_para_aniversario(7,3))      # 1
        print(dias_para_aniversario(6,3))      # 0
        print(dias_para_aniversario(5,3))      # 365

    if False:
        print('dias_para_cumpleaños')    
        print(dias_para_cumpleaños('08/10/1989'))       # 216
        print(dias_para_cumpleaños('25/10/1989'))       # 233
        print(dias_para_cumpleaños('02/05/2000'))       # 57
        print(dias_para_cumpleaños('07/03/2000'))       # 1
        print(dias_para_cumpleaños('06/03/2000'))       # 0
        print(dias_para_cumpleaños('05/03/2000'))       # 365