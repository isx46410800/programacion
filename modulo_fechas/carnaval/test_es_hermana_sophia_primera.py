# !/usr/bin/python
# -*-coding: utf-8-*-

#CAPÇALERA COMPLERTA O SERÂ UN ZERO
#Mòdul de funcions que treballen amb nombres primers
#Has de completar TOTS els docstrings
	
	
def es_primer(n):
	'''docstring'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 
	
def es_sophie_germain_primer(n):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	'''
	sophie = n * 2 + 1
	return es_primer(n) and es_primer(sophie)
		
# es_sophie_germain_primer(2)		# True
# es_sophie_germain_primer(3)		# True
# es_sophie_germain_primer(5)		# True