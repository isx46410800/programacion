# !/usr/bin/python
# -*-coding: utf-8-*-

#CAPÇALERA COMPLERTA O SERÂ UN ZERO
#Mòdul de funcions que treballen amb nombres primers
#Has de completar TOTS els docstrings
	
	
def es_primer(n):
	'''docstring'''

	num= 2
	while (n % num) != 0:
		num = num + 1
	return n == num 
	
def es_sophie_germain_primer(n):
	'''
	Funció que ens diu si un nombre qualsevol és o no un primer de Sophie 
	Germain
	es_sophie_germain_primer( int(n) )
	return bolean
	'''
	sophie = n * 2 + 1
	return es_primer(n) and es_primer(sophie)
		

def seguent_sophie_primer(n):
	'''
	Funció que donat un enter qualsevol, ens retorna el primer nombre més 
	gran que acompleix la condició dels primers de Sophie Germain 
	seguent_sophie_primer( int(n) )
	return int
	'''
	no_es_sophie = True
	cont = n
	while no_es_sophie:
		cont += 1
		if es_sophie_germain_primer(cont):
			no_es_sophie = False
	
	return cont

# seguent_sophie_primer(2) 	# 3
# seguent_sophie_primer(5) 	# 11
# seguent_sophie_primer(13) 	# 23
# seguent_sophie_primer(28) 	# 29