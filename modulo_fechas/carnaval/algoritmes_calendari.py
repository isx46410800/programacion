
#dia, mes i year són variables enteres que contenen el dia, el mes i l'any 

#algoritme per calcular el nombre del dia de la setmana 
# 0 és diumenge, 1 és dilluns, 2 és dimarts....
a = (14-mes)//12
b = year - a
c = mes - 2 + a*12 
d = b//4
e = b//100
f = b//400
g = (31*c)//12
h = dia + b + d - e + f + g
i = h % 7


#Algoritme per calcular el dia següent
#Atenció, com ja deus haver vist en la part teòrica, té un problema el 
#31 de desembre. L'has de solucionar
 
#Considerem que els mesos tnen 31 dies
ultim_dia = 31 

#el Febrer en té 28 o 29
if mes == 2: 
	if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0): 
		ultim_dia = 29 
	else: 
		ultim_dia = 28 

#abril, juny, setembre i novembre en tenen 30
elif mes == 4 or mes == 6 or mes == 9 or mes == 11: 
	ultim_dia = 30 
	
#per trobar el dia següent a l'últim dia del mes hem de fer modificacions
if dia == ultim_dia: 
	dia = 1 
	mes = mes + 1 
#si no és l'últim dia, nomès cal que sumem un al dia
else: 

	dia = dia + 1 
	
	
	
	
	
