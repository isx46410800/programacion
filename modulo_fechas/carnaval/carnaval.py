#nom autor:     jorge pastor
#data:          07/03/2019
#versió:        0.1

# 21 abril semana santa. Diumenge pasqua. Domingo primera luna llena despues del sosticio de verano
# 14 abril diumenge rams, una setmana abans de pasqua
# 6 març dimecres de cendra, 40 dies enrere de diumenge de pascua sin contar domingos
# 28   febrero dijous gras , una semana antes de dimecres de cendra. Comienza carnaval

import data


def diumenge_de_pasqua(anyo):
    """Ens retorna la data en que cau el diumenge de pasqua de l’any en qüestió
    Entrada: int positiu
    Sortida: str (data en format correcte)
    """
    
    a = anyo % 19
    b = anyo % 4
    c = anyo % 7
    a = (19 * a + 24) % 30
    b = (2 * b + 4 * c + 6 * a + 5) % 7
    dia = 22 + a + b

    mes = 3

    if dia > 31:
        dia -= 31
        mes += 1

    return data.mostrar_fecha(dia, mes, anyo)

def diumenge_rams(anyo):
    """
    F. dado un año muestra el domingo de ramos
    dijous_gras( int(año) )
    return str formato(dd/mm/yyyy)
    """
    pascua = diumenge_de_pasqua(anyo)
    dia = data.day(pascua)
    mes = data.month(pascua)
    anio = data.year(pascua)

    repetirBucle = True
    while repetirBucle: 
        diaAnterior = data.dia_anterior(dia, mes, anio)
        dia = data.day(diaAnterior)
        mes = data.month(diaAnterior)
        anio = data.year(diaAnterior)
        
        repetirBucle = data.dia_de_semana(dia, mes, anio) != 0

    return data.mostrar_fecha(dia, mes, anio)

def dimecres_cendre(anyo):
    """
    F. dado el año retorna el miercoles cenizo de ese año
    día que acaba el carnaval
    dimecres_cendre(int(dia), int(mes), int(año))
    return str formato(dd/mm/yyyy)
    """
    domingoPascua = diumenge_de_pasqua(anyo)
    dia = data.day(domingoPascua)
    mes = data.month(domingoPascua)
    anio = data.year(domingoPascua)

    cont = 1
    while cont < 40:
        if data.dia_de_semana(dia, mes, anio) != 0:
            cont += 1
            
        diaAnterior = data.dia_anterior(dia, mes, anio)
        dia = data.day(diaAnterior)
        mes = data.month(diaAnterior)
        anio = data.year(diaAnterior)

    return data.mostrar_fecha(dia, mes, anio)


def dijous_gras(anyo):
    """
    F. dado un año muestra el dia que empieza carnaval
    dijous_gras( int(año) )
    return str formato(dd/mm/yyyy)
    """
    miercolesCenizo = dimecres_cendre(anyo)
    dia = data.day(miercolesCenizo)
    mes = data.month(miercolesCenizo)
    anio = data.year(miercolesCenizo)

    while data.dia_de_semana(dia, mes, anio) != 4:
        diaAnterior = data.dia_anterior(dia, mes, anio)
        dia = data.day(diaAnterior)
        mes = data.month(diaAnterior)
        anio = data.year(diaAnterior)

    return data.mostrar_fecha(dia, mes, anio)



print(f"Pascua:\t\t {diumenge_de_pasqua(2019)}")
print(f"Domingo ramos:\t {diumenge_rams(2019)}")
print(f"Miercoles cenizo:{dimecres_cendre(2019)}")
print(f"Jueves gras:\t {dijous_gras(2019)}")