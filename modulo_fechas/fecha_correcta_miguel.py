# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si una fecha sigue el formato dd/mm/aaaa

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import time
import sys

#definimos funcion validar formato de una fecha
def validar_formato(cadena):
	'''
	entrada: str
	sortida: booleano
	'''

	#longitud correcte
	if len(cadena) != 10:
		return False
	#barras correctas
	if cadena[2] != '/' and cadena[5] != '/':
		return False
	#numeros correctos
	if not (cadena[0:2].isdigit() and cadena[3:5].isdigit() and cadena[6:10].isdigit()):
		return False
	
	return True

#----------------------------------------------------------------------------------------------------------

# funcion para data correcte
def fecha_correcta(cadena):
	'''
	Input: string
	Output: Booleano
	'''
	es_correcto = True
	if validar_formato(data):
		dia = int(data[0:2])
		mes = int(data[3:5])
		anyo = int(data[6:])
		
		if not(mes >= 1 and mes <= 12):
			es_correcto = False
			
		if mes in '1,3,5,7,8,10,12':
			if not(dia >= 1 and dia <= 31):
				es_correcto = False
				
		elif mes in '4,6,9,11':
			if not(dia >= 1 and dia <= 30):
				es_correcto = False
				
		elif mes == 2:
			if anyo % 400 == 0 or ( anyo % 4 == 0 and anyo % 100 != 0):
				if not(dia >= 1 and dia <= 29):
					es_correcto = False
					
			else:
				if not(dia >= 1 and dia <= 28):
					es_correcto = False
					
	
	return es_correcto	


#---------------------------------------------------------------------------------------------------------------------------
#definimos para saber el dia
def getdia(cadena):
	'''
	e: cadena
	s: entero
	'''
	dia = cadena[0:2]
	return int(dia)


#---------------------------------------------------------------------------------------------------------------------------

#definimos para saber el mes
def getmes(cadena):
	'''
	e: cadena
	s: entero
	'''
	mes = cadena[3:5]
	return int(mes)


#---------------------------------------------------------------------------------------------------------------------------

#definimos para saber el año
def getanyo(cadena):
	'''
	e: cadena
	s: enteros
	'''
	anyo = cadena[6:10]
	return int(anyo)
		
#---------------------------------------------------------------------------------------------------------------------------
##FUNCION ULTIMO DIA DE MES
#Definimos la funcion año de traspaso
def es_anyo_traspaso(cadena):
	'''
	entrada:str
	sortida:booleano
	'''
	anyo = getanyo(cadena)
	
	return (anyo % 400 == 0 or ( anyo % 4 == 0 and anyo % 100 != 0))

			
#definimos funcion ultimo dia del mes
def ultimo_dia_mes(cadena):
	'''
	input: str (data correcte)
	sortida: int
	'''
	ULTIMO_DIA_MES=[0,31,28,31,30,31,30,31,31,30,31,30,31]
	
	mes = getmes(cadena)
	#si es febrero i es de traspaso, modificamos el 28 por 29
	#print(es_anyo_traspaso(getanyo(cadena)), 'ooooo')
	
	if mes==2 and es_anyo_traspaso(cadena):
		ULTIMO_DIA_MES[2]=29
	
	return ULTIMO_DIA_MES[mes]
	
#---------------------------------------------------------------------------------------------------------------------------
#Funcion para el dia siguiente
def next_day(cadena):
	'''
	input: string (data correcta)
	output: int (data correcta)
	'''	
	next_dia = getdia(cadena)
	next_mes = getmes(cadena)
	next_anyo = getanyo(cadena)
	
	
	if getdia(cadena)==29 and getmes(cadena)==2 and es_anyo_traspaso(cadena):
		next_dia =1
		next_mes = getmes(cadena) + 1
		
	elif getdia(cadena)==28 and getmes(cadena)==2 and es_anyo_traspaso(cadena)==False:
		next_dia = 1
		next_mes = getmes(cadena) + 1
		
	elif getdia(cadena)==31 and getmes(cadena)==12:
		next_dia = 1
		next_mes = 1
		next_anyo = getanyo(cadena) + 1
		
	elif getdia(cadena)==31 and getmes(cadena)==1 or getmes(cadena)==3 or getmes(cadena)==5 or getmes(cadena)==7 or getmes(cadena)==8 or getmes(cadena)==10 or getmes(cadena)==12: #in '1,3,5,7,8,10,12'
		next_dia = 1
		next_mes = getmes(cadena) + 1	
		
	elif getdia(cadena)==30 and getmes(cadena)==4 or getmes(cadena)==6 or getmes(cadena)==9 or getmes(cadena)==11: # in '4,6,9,11':
		next_dia = 1
		next_mes = getmes(cadena) + 1
		
	else:
		next_dia = getdia(cadena) + 1
	
	return set_data(next_dia,next_mes,next_anyo)

def dia_siguiente(dia, mes, anio):
    """
    F. dados dia, mes y año calcula el dia siguiente
    dia_siguiente( int(dia), int(mes), int(año) )
    return str format(dd/mm/yyyy) 
    """
    # sumar un dia
    dia += 1

    # si el dia es superior al dias posibles de mes: dia a 1 y sumar un mes
    if dia > dias_mes(mes, anio) :
        dia = 1
        mes += 1

    # si el mes es superior a 12 sumar 1 año y mes a 1
    if mes > 12:
        anio += 1
        mes = 1

    return set_data(dia, mes, anio)
# def diaseguent(cadena)
	# '''
	# i: str
	# o: str
	# '''
	# #agafem dia mes i any
	# dia = getdia...
	
	# #increment dia
	# dia = dia + 1
	
	# #si passa de ultim dia, corregim
	# if dia>ultimo_dia_mes(cadena):
		# dia=1
		# mes+=1
		# if mes>12:
			# mes=1
			# anyo+=1
	# return '%02d/02d/04d'%(next_dia,next_mes,next_anyo)
	
#---------------------------------------------------------------------------------------------------------------------------

def set_data(d,m,a):
	'''
	para poner formato dd/mm/aaa
	input: 3 int
	output: 1 str data
	'''
	return '%02d/%02d/%04d'%(d,m,a)
	
#---------------------------------------------------------------------------------------------------------------------------
#funcion para conseguir la fecha y hora del sistema
import datetime
def fecha_sistema():

	ahora = datetime.datetime.now()
	return '%02d/%02d/%04d'%(ahora.day,ahora.month,ahora.year)
	
#---------------------------------------------------------------------------------------------------------------------------
#funcion para saber dias al aniversario
def aniversario(fecha):
	#leer dia y mes
	dia=getdia(fecha)
	mes=getmes(fecha)
	anyo=getanyo(fecha)

	#Extraigo fecha actual
    fecha_actual = fecha_sistema()
	
	#coger la data del sistema
	dia_sistema=getdia(fecha_actual)
	mes_sistema=getmes(fecha_actual)
	anyo_sistema=getanyo(fecha_actual)
	
	cumple=''
	
	#si ha pasado:
	if dia<dia_sistema and mes==mes_sistema:
		#la fecha del año que viene
		cumple=set_data(dia, mes, anyo_sistema+1)
		
	#sino ha pasado, contamos los dias que faltan	
	else:	
		dia=dia_sistema-dia
		cumple=set_data(dia, mes, anyo_sistema)	
		
	#return cumple
	return cumple
	
# def compara_data(d1,d2):
	# '''
	# '''
	
	# return int(1 si d1>d2, 0 si son igual, -1 si d1<d2)



#--------------------------TESTS DRIVER--------------------------------------------
if __name__=='__main__':
	if False:
		#TEST DRIVER DE DECIR SI ES DE TRASPASO O NO	
		print(es_anyo_traspaso("10/02/2000"))	
		print(es_anyo_traspaso("10/02/2001"))	
		print(es_anyo_traspaso("10/03/2100"))	

		#TEST DRIVER DE DECIRME EL DIA
		print(getdia('10/02/2000'))
		print(getdia('10/03/2010'))

		#TEST DRIVER DE DECIRME EL MES
		print(getmes('10/02/2000'))
		print(getmes('10/03/2010'))

		#TEST DRIVER DE DECIRME EL AÑO
		print(getanyo('10/02/2000'))
		print(getanyo('10/03/2010'))

		#TEST DRIVER DE DECIRME EL ULTIMO DIA DEL MES DE CADA MES SEGUN EL AÑO	
		print(ultimo_dia_mes('10/03/1993'))
		print(ultimo_dia_mes('10/02/2000'))
		print(ultimo_dia_mes('10/02/2001'))
		print(ultimo_dia_mes('10/04/2000'))

		#TEST DRIVER SIGUIENTE DIA
		print(next_day("10/02/2000"))
		print(next_day("29/02/2000"))
		print(next_day("10/04/2000"))
		print(next_day("31/03/2000"))
		print(next_day("30/04/2000"))
		print(next_day("31/12/2000"))
		
		#TEST DRIVER FECHA ACTUAL
		print(fecha_sistema())

		
	if False:
		#TEST DRIVER FORMATO CORRECTO
		print(validar_formato("10/02/2000"))
		print(validar_formato("10-02-2000"))
		print(validar_formato("1/02/2000"))
		
		#TEST DRIVER FECHA CORRECTA
		print(fecha_correcta("10/02/2000"))
		print(fecha_correcta("40/02/2000"))
		print(fecha_correcta("10/13/2000"))
		print(fecha_correcta("10/13/20001"))
		
		#TEST DRIVER SET DATA
		print(set_data(5,10,2001))
		print(set_data(5,1,2001))
		
	if True:
		#TEST DRIVER FECHA ACTUAL
		print(fecha_sistema())
		#TEST DRIVER ANIVERSARI
		print(aniversario("24/03/2019"))
		print(aniversario("14/05/2019"))
		print(aniversario("14/03/2020"))


#---------------------------------------------------------------------------------------------------------------------------


	
	
	
	
	
	
	
	
	
	
