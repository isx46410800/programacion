# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si una fecha sigue el formato dd/mm/aaaa

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos

#data correcte
def data_correcte(data):
	'''
	Input: string
	Output: Booleano
	'''
	correcte = True
	if data_format_correcte(data):
		dia = int(data[0:2])
		mes = int(data[3:5])
		anyo = int(data[6:])
		
		if not(mes >= 1 and mes <= 12):
			correcte = False
			
		if (mes == 1 or mes == 3 or mes == 5 or mes == 7 or mes == 8 or mes == 10 or mes == 12):
			if not(dia >= 1 and dia <= 31):
				correcte = False
				
		elif (mes == 4 or mes == 6 or mes == 9 or mes == 11):
			if not(dia >= 1 and dia <= 30):
				correcte = False
				
		elif mes == 2:
			if anyo % 400 == 0 or ( anyo % 4 == 0 and anyo % 100 != 0):
				if not(dia >= 1 and dia <= 29):
					correcte = False
					
			else:
				if not(dia >= 1 and dia <= 28):
					correcte = False
					
	
		
	return correcte	
