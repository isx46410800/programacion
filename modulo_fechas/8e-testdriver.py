#definimos funcion validar formato de una fecha
def validar_formato(cadena):
	'''
	entrada: str
	sortida: booleano
	'''

	#longitud correcte
	if len(cadena) != 10:
		return False
	#barras correctas
	if cadena[2] != '/' and cadena[5] != '/':
		return False
	#numeros correctos
	if not (cadena[0:2].isdigit() and cadena[3:5].isdigit() and cadena[6:10].isdigit()):
		return False
	
	return True

#TEST DRIVER DE CADA FUNCION, PONER PRINT CON EJEMPLOS, ESTILO JUEGO DE PRUEBAS
print(validar_formato('14/03/1993'))
print(validar_formato('14/03/19933'))
print(validar_formato('1431993'))
print(validar_formato('patata'))
print(validar_formato('14-03-1993'))

