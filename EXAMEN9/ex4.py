#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#EXAMEN LLISTES I MODULS 
#EXERCICI 4 - definir la funcion convierta las fechas en enteros y ordenadarlas
#------------------------------------------
import sys
import funcions_dates

def data_a_num(data):
	
	'''
	entrada: str data format correcta
	sortida: int
	'''
	
	llista_nums = []
	#obtenim el mes i els dias i l'any
	dia = funcions_dates.get_day(data)
	mes = funcions_dates.get_month(data)
	y = funcions_dates.get_year(data)
	
	# establim la data per a calcular els mesos que han pasat
	mes = mes - 1
	data = funcions_dates.set_date(dia,mes,y)
	count = 0	
	
	# calculem els dias que han anat pasant
	while mes > 0:
		
		ultim_dia = funcions_dates.last_day_of_month(data)
		count = count + ultim_dia
		mes = mes - 1
		data = funcions_dates.set_date(dia,mes,y)
	
	count = count + dia	
	
	# obtenim la llista amb la data pasada a int
		
	return count

#PROGRAMA
list_data = sys.argv[1:]

llista_numeros=[]

# per cada data de la llista la pasem a int
for data in list_data:
	
	# contem els dias que han pasat desde principi d'any
	
	count = data_a_num(data)
		
	llista_numeros.append(count)

# ordenem la llista de nombres que tenim

llista_ordenada = funcions_dates.bombolla(llista_numeros)

# mostrem el resultat
print (llista_ordenada)

if __name__ == '__main__':
#TEST DRIVERS    
   if True:
	   print(data_a_num('05/03/1997'))#64
	   print(data_a_num('08/05/1999'))#128
	   print(data_a_num('15/11/2005'))#319
	   #print(data_a_num('05/03/1997','06/03/1997','10/10/2000'))#[64, 65, 284]
	   
