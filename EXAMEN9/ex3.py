#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#EXAMEN LLISTES I MODULS 
#EXERCICI 3 - definir la funcion bombolla
#------------------------------------------

#definimos la funcion bombolla
def bombolla(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista d'enters
	Sortida: llista d'enters
	'''
	#bucle para el rango de la lista de enteros
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		#miramos las posicion para ordenarlo de menor a mayor
		for j in range(0, final): 
			if (llista[j] > llista[j+1]):
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	#return
	return llista


#TEST DRIVER
if __name__ == "__main__":
#bombolla fechas
	if True:
		print(bombolla([1,5,2])) #[1,2,5]
		print(bombolla([10,5])) #[5,10]
		print(bombolla([11,-5,2,7])) #[-5,2,7,11]
		print(bombolla([10,50,20,7])) #[7,10,20,50]
