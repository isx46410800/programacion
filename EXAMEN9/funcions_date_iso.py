#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#EXAMEN LLISTES I MODULS 
#EXERCICI 6 - definir un modulo nuevo con el formato ISO
#
#------------------------------------------
import datetime

def correct_format(a):

    '''
    Given a str that represents a date,
    it verifies that is a correct date (YYYY-MM-DD)
    INPUT = str that represents a date
    OUTPUT = Bool
    '''

    if not (len(a) == 10):

        return False

    num = a[0:4] + a[5:7] + a[8:10]
    are_slash = a[4] == "-" and a[7] == "-"

    if not (num.isdigit() and are_slash):

        return False

    return True

def get_day(a):

    '''
    Given a str that represents a correct froamt date (DD/MM/YYYY),
    extracts de day of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    day = int(a[8:10])

    return day

def get_month(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de month of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    month = int(a[5:7])

    return month

def get_year(a):

    '''
    Given a str that represents a correct date (DD/MM/YYYY),
    extracts de year of the input date.
    INPUT = str (correct date form)
    OUTPUT = int
    '''

    year = int(a[0:4])

    return year
    
def leap_year(a):

    '''
    Given a int that represents a year, says if its a leap year or not
    INPUT= int > 0 (year)
    OUTPUT = BOOL
    '''

    return (a % 400 == 0 or (a % 4 == 0 and not a % 100 == 0))

def last_day_of_month(month, year):

    '''
    Says the last day of that month.
    INPUT = int (correct month), int(year)
    OUTPUT= int
    '''

   

    last_days = [0,31,28,31,30,31,30,31,31,30,31,30,31]

    # IF ITS FEBRUARY AND A LEAP-YEAR

    if month == 2 and leap_year(year):

        last_days[2] = 29

    return last_days[month]
    
def valid_date(a):

    '''
    Given a str that represents date, says if the input string is a correct date.
    INPUT = str in the form of XXXX-XX-XX where X is an int
    OUTPUT = Bool
    '''

    if not correct_format(a):

        return False

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)


    if year < 0:

        return False

    if not (month >= 1 and month <= 12):

        return False

    if not (day >= 1 and day <= last_day_of_month(month, year)):

        return False

    return True

def set_date(y,m,d):

    '''
    Given 3 ints > 0 that represent a day, month and year respectively,
    creates a str in the form of DD/MM/YYYY
    INPUT = 3 ints > 0
    OUTPUT = 1 str (DD/MM/YYYY)
    '''

    return f"{y:04}-{m:02}-{d:02}"
    


def today():

    '''
    Function that returns the current date like YYYY-MM-DD
    INPUT = None
    OUTPUT = str (correct date YYYY-MM-DD
    '''
    t = datetime.datetime.now()
    return set_date(t.year,t.month,t.day)
    
def next_day(a):

    '''
    Given a str that represents a corrent date,
    it returns the following day.
    INPUT = str (correct date YYYY-MM-DD)
    OUTPUT = str (correct date YYYY-MM-DD)
    '''

    # Extract day, month and year

    day = get_day(a)
    month = get_month(a)
    year = get_year(a)

    # Increase the day by 1

    day += 1

    # Check if the day is a correct date and make the necessary changes

    # If the day is not corect

    if day > last_day_of_month(month, year):

        day = 1
        month += 1

    # If the month is not correct

    if month > 12:

        month = 1
        year += 1

    return set_date(year,month,day)

def date_comparator(date1, date2):

    '''
    Function that given 2 correct dates (in the form of DD/MM/YYYY),
    returns an int (-1 or 0 or 1) depending on whether the first one
    is an earlier date (-1), it's the same date (0)
    or the second one is a later date (1).
    '''

    status=1

    d1_day= get_day(date1)
    d1_month= get_month(date1)
    d1_year= get_year(date1)

    d2_day= get_day(date2)
    d2_month= get_month(date2)
    d2_year= get_year(date2)

    if d1_year < d2_year:

        status = -1

    elif d1_year == d2_year:

        if d1_month < d2_month:

            status = -1

        elif d1_month == d2_month:

            if d1_day < d2_day:

                status = -1

            elif d1_day == d2_day:

                status = 0

    return status
    
#definimos la funcion bombolla dates
def bombolla_dates(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista de dates
	Sortida: llista de datas ordenada
	'''
	#bucle para el rango de la lista de enteros
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		#miramos y comparamos para ordenarlo de menor a mayor
		for j in range(0, final): 
			if funcions_dates.date_comparator(llista[j],llista[j+1]) == 1:
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	return llista

#print(get_day('1993-03-14'))
    
if __name__ == '__main__':

    #TEST DRIVES

    if True:

        print('correct format')
        print(correct_format('3')) #False
        print(correct_format('08/061/997')) #False
        print(correct_format('08/06/1997')) #False
        print(correct_format('1993-03-14')) #True

    #if False:

        #print('leap_year')
        #print(leap_year(2008)) #True
        #print(leap_year(2007)) #False

    #if True:

        #print('get_day')
		#print(get_day('1993-03-14'))#14
