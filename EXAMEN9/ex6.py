#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#EXAMEN LLISTES I MODULS 
#EXERCICI 6
#------------------------------------------

import sys
import funcions_date_iso

#Programa que donades unes dates en format YYYY-MM-DD les ordena

list_data = sys.argv[1:]

# control de errors

if len(sys.argv) < 2:
	print('Numero de args incorrecte')
	exit(1)

for data in list_data:
	
	if not funcions_date_iso.valid_date(data):
		
		print('alguna de las datas no seguiex el format YYYY-MM-DD')
		exit(1)

# programa

llista_ordenada = funcions_date_iso.bombolla_dates(list_data)

print(llista_ordenada)
