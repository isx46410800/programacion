#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#MIGUEL AMOROS ISX46410800
#EXAMEN LLISTES I MODULS 
#EXERCICI 3
#------------------------------------------
import funcions_dates
import sys

#definimos la funcion bombolla dates
def bombolla_dates(llista):
	'''
	Ordena una llista de menor a major mitjançant 
	l'algorisme bombolla
	Entrada: llista de dates
	Sortida: llista de datas ordenada
	'''
	#bucle para el rango de la lista de enteros
	for i in range(1, len(llista)): 
		final = len(llista) - i 
		#miramos y comparamos para ordenarlo de menor a mayor
		for j in range(0, final): 
			if funcions_dates.date_comparator(llista[j],llista[j+1]) == 1:
				t = llista[j] 
				llista[j] = llista[j+1] 
				llista[j+1] = t 
				
	return llista

##PROGRAMA
#donades una llista de dates les ordena

list_data = sys.argv[1:]

llista_ordenada = funcions_dates.bombolla_dates(list_data)

print(llista_ordenada)



#TEST DRIVER
if __name__ == "__main__":
	if False:
		#COMPARARAMOS DATOS
		print (funcions_dates.date_comparator('09/02/2017','08/02/2017')) #1
		print (funcions_dates.date_comparator('08/02/2017','09/02/2017')) #-1
		print (funcions_dates.date_comparator('08/02/2017','08/02/2017')) #0
		print (funcions_dates.date_comparator('09/02/2016','08/02/2017')) #-1
		print (funcions_dates.date_comparator('09/02/2017','08/02/2016')) #1
		print (funcions_dates.date_comparator('09/03/2017','08/02/2017')) #1
		print (funcions_dates.date_comparator('09/02/2017','08/03/2017')) #-1
		#bombolla dates
	if False:
		print(bombolla_dates(['08/03/2017','09/02/2017','09/02/2016'])) #['09/02/2016', '09/02/2017', '08/03/2017']
		print(bombolla_dates(['09/02/2017','08/03/2017','09/02/2017','09/02/2016','08/02/2017','09/02/2017','08/02/2017'])) #['09/02/2016', '08/02/2017', '08/02/2017', '09/02/2017', '09/02/2017', '09/02/2017', '08/03/2017']
