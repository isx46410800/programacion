#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#LLEGIR UN TEXT I COMPTAR LA FREQ DE CADA PARAULA


import modulos_llistas

#introducimos el texto
text = input()
#normalizamos el texto
text= modulos_llistas.normalizar_texto(text)
#del texto, lo pasamos a lista y su calculo de freqs con la funcion
frecuencia=modulos_llistas.contar_palabras(text)
print(frecuencia)

#mostramos el histograma
modulos_llistas.histograma(frecuencia[0], frecuencia[1])

