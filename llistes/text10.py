
def histograma(llistaelem, llistatotal):
	''' funcio histograma
	enttrada:llista de str i llista de nums amb la mateixa longitud
	sortida: res '''
	
	caracter = '*'

	pos = 0	

	for k in llistatotal:
		asterisc = ''
		element = llistaelem[pos] 
		
		for i in range(0,k):
			asterisc = asterisc + caracter
		
		pos = pos + 1	
		print (element, asterisc)

# ----------------------------------------------------------------------
def busqueda_sequencial(llista, element): 
	''' en quina posicio hi ha un element determinat, -1 si es falç
	entrada: llista, element
	sortida: int'''
				
	pos = 0

	while pos < len(llista):
		
		if llista[pos] == element:
			encontrado = True
			return pos
		else:
			pos = pos + 1
			
	pos = - 1
		
	return pos

# ----------------------------------------------------------------------

def llista_paraules(llista):
	
	'''entrada: llista de str
       sortida: llista_final(llista de paraules(str) i llista de freqs(ints)'''
       
	llista_final = []
	llista_final_count = []
	
	# per cada element de la llista
       
	for element in llista:
		
		contador = 1
		
		# si el element esta en la llista
		
		if element in llista_final:
					
			pos = busqueda_sequencial(llista_final,element)
			llista_final_count[pos] = llista_final_count[pos] + 1
			
		# si el element no esta en la llista
			
		else:
			
			llista_final.append(element)	
			llista_final_count.append(contador)
	
	llista_total = [llista_final, llista_final_count]
	return llista_total
	
# ----------------------------------------------------------------------

text = input()

llista = text.split()

# obtenim la llista final

llista_final = llista_paraules(llista)

# fem l'histograma

histograma(llista_final[0], llista_final[1])

