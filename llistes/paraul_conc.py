#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
import sys
#1.contar palabras concretas
#funcio busca posicio
def cerca_sequencial (llista , element):
	'''
	Retorna la 1a posicio d'un element a una llista. (-1 si no hi es)
	
	Entrada: list element , element
	Sortida: int
	'''
	
	for i in range(0,len(llista)):
				if element == llista[i]:
					return i
	
	return -1
#funcio retorna histograma
def histograma(list1,list2):
	'''
	Input: two lists
	Output: a unique list
	'''
	solucio_histograma = ""
	
	for pos in range(0,len(list1)):
		
		caracter = '*'
		
		frequencia = list2[pos] * caracter
		
		letter=f"{list1[pos]}: {frequencia}({list2[pos]})"
		
		solucio_histograma = solucio_histograma + "\n" + letter + "\n"
		
	return solucio_histograma
#funcio arreglar text
def treu_signes(cadena):
	'''
	'''
	SIGNES=[',','!','?',';','.',':']
	nova_cadena=''
	for lletra in cadena:
		
		if lletra in SIGNES:
			nova_cadena = nova_cadena + ' '
		else:
			nova_cadena = nova_cadena + lletra
			
	return nova_cadena

text=input()
#treure signes
text=treu_signes(text)
#pasar text a llista de paraules
llista_paraules=text.split()
PARAULES=['Hola','adeu']
contador=[0,0]
for paraula in llista_paraules:
			
	if paraula in PARAULES:
		
		posicio=cerca_sequencial(PARAULES,paraula)
		
		contador[posicio] = contador[posicio] + 1
			
print(histograma(PARAULES,contador))

		
