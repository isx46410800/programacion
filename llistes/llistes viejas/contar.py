#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
#1. Fes un programa que cuente vocales de una frase
#-----------------------------------------------------
# def contar_vocales(texto):
	# '''
	# entrada: cadena de texto
	# output: lista de recuento de vocales
	# '''
	# VOCALES=['a','e','i','o','u']
	# contador=[0,0,0,0,0]
	
	# #por cada letra del texto
	# for c in texto:
		# #si es vocal
		# if c in VOCALES:
			# #buscar donde esta
			# for pos in range():
				
	
				
# def ver_vocales(texto):
	# '''
	# funcion que te dice las vocales que hay de un texto
	# input= string
	# output= lista de vocales
	# '''
	# VOCALES=['a','e','i','o','u']
	
	# llista=[]
	
	# for vocal in texto:
		# if vocal in VOCALES:
			# llista.append(vocal)
			
	# return llista
import sys

def buscar_posicion(llista_vocales, vocal):
	'''
	entrada: lista de strings y un string a buscar
	output: un int de posicion
	'''
	contador=0
	posicion=0
	
	for char in llista_vocales:
		if char==vocal:
			posicion=contador
		posicion +=1
		
	return posicion
	
def contar_vocales(texto):
	'''
	input: cadena de strings
	output: int
	'''
	vocales=['a','e','i','o','u']
	cont_freq=[0,0,0,0,0]
	
	llista=[]
	
	for char in texto:
		if char in vocales:
			posicion=buscar_posicion(vocales,char)
			cont_freq[posicion] +=1
				
	return cont_freq
	
def histograma(vocales, llista):
	'''
	dibuixa un histograma horitzontal
	entrada: llista de str, llista enteros positius
	sortida: none
	'''		
	CARACTER='*'
	for pos in range(0, len(vocales)):
		print(f"{vocales[pos]:4}{CARACTER*llista[pos]}")
	
	return
	
#programa
texto=sys.argv[1]
vocales=sys.argv[2]
frequencia=contar_vocales(texto)
histograma(vocales,frequencia)

if __name__ == '__main__':
	if True:
		#print(buscar_posicion(['a','e','i','o','u'], 'e'))
		print(contar_vocales("Me llamo Miguel")) #[1,2,1,1,1]
		print(contar_vocales("Me llamo miguel amoros")) #[2,2,1,3,1]
		print(contar_vocales("mmm")) #[0,0,0,0,0]
		print(contar_vocales("")) #[0,0,0,0,0]
		#print(histograma('aeiou', [2,2,2,2,2]))
		
		
