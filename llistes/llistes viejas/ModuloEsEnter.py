# !/usr/bin/python3
# -*-coding: utf-8-*-

#nom autor Miguel Amorós Moret
#isx 46410800
#data 12/12/2018
#versió 1


#descripció : con funcion decir si es entero o no

#Especificacions d'entrada i joc de proves: EE: UN NUMERO ENTER 

#importamos para pasar argumentos
import sys


#definimos la funcion es entero
def es_entero(cadena):
	'''
	e: cadena
	s: booleano
	'''
	#quitamos signo si hace falta
	if cadena[0] in '+-':
		cadena = cadena[1:]
	#si todo son digitos
	return cadena.isdigit()


if __name__=='__main__':
	if False:
		#leemos argumento
		cadena = sys.argv[1]

		#llamamos a la funcion
		print(es_entero(cadena))


