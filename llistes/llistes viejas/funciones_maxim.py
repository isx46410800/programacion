#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
#1. Fes un programa que mostri el màxim dels nombres entrats com a argument.
#       Quines funcions et semblen necessàries?
#-----------------------------------------------------
import sys

def maxim_numero(llista):
	'''
	entrada: llista enters no buida
	salida:int
	'''
	
	#llista=str_to_int(llista)
	maxim=llista[0]
	for element in llista:
		
		if element > maxim:
			maxim=element
	return maxim
	
def str_to_int(llista):
	'''
	entrada: llista de str
	sortida: llista de int
	'''
	#creamos lista vacia de enteros
	llista_int=[]
	
	#vamos pasando cada elemento de la lista de strings y lo añadimos a la lista
	for element in llista:
		llista_int.append(int(element))
		#llista_int = llista_int + int(element) otra opcion
		
	return llista_int
	
#programa

#entrada
l= sys.argv[1:]

#conversion a enteros
l_int = str_to_int(l)

#printamos el maximo
print(maxim_numero(l_int))

#-------------TEST DRIVER------------------------
if __name__=='__main__':
	if True:
		#Calculo del numero maximo
		print(maxim_numero([3,5,7,1])) #7
		print(maxim_numero([-3,52,9,-1])) #52
		print(maxim_numero([-3,-2,-9,-1])) #-1
		
	if False:	
		#dar una lista de str a int
		print(str_to_int(['3','5','2']))
		print(str_to_int(['-3','-5','-2']))
		
