#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
#   4. Fes un programa que et demani un nombre i escrigui la llista de tots els divisors primers fins a ell.
#-----------------------------------------------------

num=int(input())

es_primo=True

i=2

while i<=num:
	if num%i==0:
		es_primo=True
		j=2
		while j<i and es_primo:
			if i%j==0:
				es_primo=False
			j=j+1
			
		if es_primo:
			print(i)
	i=i+1
