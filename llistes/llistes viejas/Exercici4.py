#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
#   4. Fes un programa que et demani un nombre i escrigui la llista de tots els divisors primers fins a ell.
#-----------------------------------------------------
#creamos lista vacia
llista=[]


#indicamos la entrada del numero
num=int(input())

#iniciamos dividor
i=2

es_primo=True

#bucle para indicar los divisores
while i <=num:
	#Si es divisor, lo añadimos a la lista si ese divisor es primo.
	if num%i==0:
		es_primo=True
		j=2
		while j<i:
			if i%j==0:
				es_primo=False
			j=j+1
		if es_primo:
			llista.append(i)		
	i=i+1
	
#mostramos resultado de la lista
print(llista)

