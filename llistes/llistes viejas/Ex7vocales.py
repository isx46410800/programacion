#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
#1. Fes un programa que cuente vocales de una frase
#-----------------------------------------------------

		
#Descripció: Fes un programa que compti cada una de les vocals d'una frase

# E.E: Nombres enters majors que 0 n>0
# Joc de proves:
# Entrada   |   Sortida
#-------------------------
# `Hola bon dia`   |   2, 0, 1, 2, 0 

import sys

def cerca_seq(l,element):
    '''
    Def: Troba la posició del element especificat en una llista
    Entrada: str (llista), str (element)
    Sortida: int (posicio)
    '''
    posicio=0
    cont=0
    # Iterem la llista cercant l'element
    for char in l:
        if char == element:
            posicio = cont
        cont+=1
    
    return posicio

def compta_freq(text):
    '''
    Def: Llegeix un text i retorna el número de cops que apareix x
    Entrada: str (text)
    Sortida: llista de freqüències
    '''
    vocals=["a", "e", "i", "o", "u"]
    l_freq=[0, 0, 0, 0, 0]
    for char in text:
        if char in vocals:
            # Treiem la posició a la llista i la vocal que és
            posicio = cerca_seq(vocals, char)
            l_freq[posicio]+=1
    return l_freq
    
    
# def compta_freq(text,cas):
	# '''
	# '''
	# l_freq = [0] * len(cas)
	# for char in text:
		# if char in cas:
			# posicio = cerca_seq[cas,char]    
			
def histograma(l_caps, l_freq):
	'''
	dibuixa un histograma horitzontal
	entrada: llista de str, llista enteros positius
	sortida: none
	'''		
	CARACTER='*'
	for pos in range(0, len(l_caps)):
		print(f"{l_caps[pos]:4}{CARACTER*l_freq[pos]}")
	
	return
	

#programa
texto=sys.argv[1]
CARACTERS=sys.argv[2]
frequencia=compta_freq(texto)
histograma(CARACTERS,frequencia)

if __name__ == '__main__':
	if True:
		print(cerca_seq()
		print(compta_freq("Me llamo Miguel")) #[1,2,1,1,1]
		print(compta_freq("Me llamo miguel amoros")) #[2,2,1,3,1]
		print(compta_freq("mmm")) #[0,0,0,0,0]
		print(compta_freq("")) #[0,0,0,0,0]	
    
				
