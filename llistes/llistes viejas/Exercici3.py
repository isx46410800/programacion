#!/usr/bin/env python!
# -*- coding: utf-8 -*-
#LLISTES 
# 3. Fes un programa que et demani un nombre enter positiu i a continuació faci una llista amb tots els divisors del nombre (inclosos el 1 i ell mateix)
#-----------------------------------------------------
#creamos lista vacia
llista=[]

#indicamos la entrada del numero
num=int(input())

#iniciamos dividor
i=1

#bucle para indicar los divisores
while i <=num:
	#Si es divisor, lo añadimos a la lista
	if num%i==0:
		llista.append(i)
	i=i+1
	
#mostramos resultado de la lista
print(llista)


