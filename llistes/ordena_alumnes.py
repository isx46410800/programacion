#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#ORDENA ALUMNES
##Fes que el programa ordena_alumnes.py funcioni, definint les funcions d'acord al seu docstring i fent que la sortida s'assembli a això:
#alumnes sense ordenar:
#jordi perez, dni: 12345678A, mitjana: 6
#manel garcia, dni: 52345678A, mitjana: 2
#...

#definimos funcion para calcularl el apellido ordenado
def ordenar_cognom(llista_alumno):
	'''
	input:nos da una lista de str
	ouput: str de cognom
	'''
	#obtenemos campo nombre separado
	lista_nombre=llista_alumno[1].split()
	#print(lista_nombre)
	nombre=lista_nombre[0]
	apellido=lista_nombre[1]
	##(nom,cognom)=lista_nombre.split()--->asigna nom el 0 y cognom 1
	
	return apellido
	#tupla para desempatar por apellido
	##return (cognom,nom)
	
def ordenar_mitjana(llista_alumno):
	'''
	input:lista de str
	output: float
	'''
	total_nota=0
	for pos in range(0, len(llista_alumno[2])):
		total_nota= total_nota + llista_alumno[2][pos]
	
	mitjana=total_nota//len(llista_alumno[2])	
	
	#para que salga sustituido el campo de las notas por la nota mitjana calculada [xx,xx,[2,2,2,2]]->[xx,xx,2.0]
	llista_alumno[2]=mitjana
	
	return mitjana
	
#lista general de alumnos
dadesAlumnes = [['12345678A', 'jordi perez', [5.2, 8, 9, 4.3]], ['52345678A',
	'manel garcia', [2, 1.5, 3, 5]], ['62345678A', 'joan-ramon vila', [7,7,8,10]],
	 ['72345678A', 'maria casas', [7, 2.5, 9, 3]], ['111111111D', 'josep-lluís márquez',
	 [3, 10, 5]]]


#creamos una lista vacia para cada alumno
nueva_lista=[]

#para cada alumno, cogemos sus campos por separados y calculamos la nota
for i in range(0, len(dadesAlumnes)):
	nueva_lista=dadesAlumnes[i]
	#obtenemos los diferentes campos de cada alumno
	##print(nueva_lista[0], nueva_lista[1], nueva_lista[2])
	
	#obtenemos dni
	dni=nueva_lista[0]
	
	#obtenemos campo nombre separado
	lista_nombre=nueva_lista[1].split()
	#print(lista_nombre)
	nombre=lista_nombre[0]
	apellido=lista_nombre[1]
	
	#calculamos la nota mitjana
	total_nota=0
	for pos in range(0, len(nueva_lista[2])):
		total_nota= total_nota + nueva_lista[2][pos]
	
	mitjana=total_nota//len(nueva_lista[2])	
	
	#mostramos resultado por cada alumno
	print(nombre, apellido, "dni:", dni, "mitjana:", mitjana)
		

#ordena la lista segun el primer campo, sort normal.	
dadesAlumnes.sort()
print(dadesAlumnes)

#ordena la lista de alumnos segun apellido, cogemos el apellido segun la funcion
dadesAlumnes.sort(key=ordenar_cognom)
print(dadesAlumnes)

#ordena la lista de alumnos segun mitjana, cogemos la mitjana segun la funcion
dadesAlumnes.sort(key=ordenar_mitjana)
print(dadesAlumnes)



#------------------------------------------
# def alumne_tostring(l_alumne):
	# '''
	# '''
	
	# return nombre, apellido, "dni:", dni, "mitjana:", mitjana
	
# def mitjana(e):
	# '''
	# e:llista d'enters
	# s:int
	# '''
	
	# return mitjana
	
# def llistat(l_alumne):
	# '''
	# e: llista alumnes (un alumne es [...])
	# s:res
	# '''
	# for alumne in l_alumne:
		# print(alumne_to_string(alumne))


# #alumnes sense ordenar
# #llistat(dadesAlumnes)
	# #per cada alumne
	
			# #mostra'l
			

# #alumnes ordenats per cognom
# dadesAlumnes.sort(key=ordenar_cognom)
	# #per cada alumne
	# #mostra'l
# llistat(dadesAlumnes)
			
