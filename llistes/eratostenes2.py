# !/usr/bin/python
# -*-coding: utf-8-*-
# Pere Canet
# isx41747980
##############################
import sys
import funcio_eratostenes
#programa2

#obtenim de quants elements serà la llista
fi_llista = int(sys.argv[1])

#cream la llista amb el numero de numeros introduit
cont = 3
llista = [2]
while cont <= (fi_llista+1):

    llista.append(cont)
    cont += 1
print(llista)
#calculem fins on hem de recorrer, la meitat
meitat = 0
if len(llista)%2!=0:

    meitat = (len(llista)+1)//2

else:

    meitat = len(llista) // 2

#recorrem la llista element a element
for pos in range(0, meitat):

    #per cada element tatxam els seus multiples
    element = int(llista[pos])
    if element != 0:

        funcio_eratostenes.tatxarMultiples(llista, element, pos)
        

print(llista)
