#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#LLEGIR UN TEXT I COMPTAR PARAULES CONCRETES

import sys
import modulos_llistas

#entra de texto
texto=input()

texto=texto.split()

palabras=['adios','miguel']

frecuencia=modulos_llistas.contador_segun_palabras(palabras,texto)

modulos_llistas.histograma(palabras, frecuencia)

